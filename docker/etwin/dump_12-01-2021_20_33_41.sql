--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1 (Debian 13.1-1.pgdg100+1)
-- Dumped by pg_dump version 13.1 (Debian 13.1-1.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: announcement_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.announcement_id AS uuid;


ALTER DOMAIN public.announcement_id OWNER TO mysql;

--
-- Name: dinoparc_server; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.dinoparc_server AS character varying(15)
	CONSTRAINT dinoparc_server_check CHECK (((VALUE)::text = ANY (ARRAY[('dinoparc.com'::character varying)::text, ('en.dinoparc.com'::character varying)::text, ('sp.dinoparc.com'::character varying)::text])));


ALTER DOMAIN public.dinoparc_server OWNER TO mysql;

--
-- Name: dinoparc_session_key; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.dinoparc_session_key AS character varying(32)
	CONSTRAINT dinoparc_session_key_check CHECK (((VALUE)::text ~ '^[0-9a-zA-Z]{32}$'::text));


ALTER DOMAIN public.dinoparc_session_key OWNER TO mysql;

--
-- Name: dinoparc_user_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.dinoparc_user_id AS character varying(10)
	CONSTRAINT dinoparc_user_id_check CHECK (((VALUE)::text ~ '^[1-9]\d{0,9}$'::text));


ALTER DOMAIN public.dinoparc_user_id OWNER TO mysql;

--
-- Name: dinoparc_username; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.dinoparc_username AS character varying(20);


ALTER DOMAIN public.dinoparc_username OWNER TO mysql;

--
-- Name: forum_thread_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.forum_thread_id AS uuid;


ALTER DOMAIN public.forum_thread_id OWNER TO mysql;

--
-- Name: hammerfest_server; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.hammerfest_server AS character varying(13)
	CONSTRAINT hammerfest_server_check CHECK (((VALUE)::text = ANY (ARRAY[('hammerfest.es'::character varying)::text, ('hammerfest.fr'::character varying)::text, ('hfest.net'::character varying)::text])));


ALTER DOMAIN public.hammerfest_server OWNER TO mysql;

--
-- Name: hammerfest_session_key; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.hammerfest_session_key AS character varying(26)
	CONSTRAINT hammerfest_session_key_check CHECK (((VALUE)::text ~ '^[0-9a-z]{26}$'::text));


ALTER DOMAIN public.hammerfest_session_key OWNER TO mysql;

--
-- Name: hammerfest_user_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.hammerfest_user_id AS character varying(10)
	CONSTRAINT hammerfest_user_id_check CHECK (((VALUE)::text ~ '^[1-9]\d{0,9}$'::text));


ALTER DOMAIN public.hammerfest_user_id OWNER TO mysql;

--
-- Name: hammerfest_username; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.hammerfest_username AS character varying(20)
	CONSTRAINT hammerfest_username_check CHECK (((VALUE)::text ~ '^[0-9A-Za-z]{1,12}$'::text));


ALTER DOMAIN public.hammerfest_username OWNER TO mysql;

--
-- Name: instant; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.instant AS timestamp(3) with time zone;


ALTER DOMAIN public.instant OWNER TO mysql;

--
-- Name: locale_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.locale_id AS character varying(10);


ALTER DOMAIN public.locale_id OWNER TO mysql;

--
-- Name: twinoid_user_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.twinoid_user_id AS character varying(10)
	CONSTRAINT twinoid_user_id_check CHECK (((VALUE)::text ~ '^[1-9]\d{0,9}$'::text));


ALTER DOMAIN public.twinoid_user_id OWNER TO mysql;

--
-- Name: user_id; Type: DOMAIN; Schema: public; Owner: mysql
--

CREATE DOMAIN public.user_id AS uuid;


ALTER DOMAIN public.user_id OWNER TO mysql;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: _post_formatting_costs; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public._post_formatting_costs (
    forum_post_revision_id uuid NOT NULL,
    formatting character varying(20) NOT NULL,
    cost integer,
    CONSTRAINT _post_formatting_costs_cost_check CHECK ((cost > 0))
);


ALTER TABLE public._post_formatting_costs OWNER TO mysql;

--
-- Name: announcements; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.announcements (
    announcement_id public.announcement_id NOT NULL,
    forum_thread_id public.forum_thread_id NOT NULL,
    locale public.locale_id,
    created_at public.instant NOT NULL,
    created_by public.user_id NOT NULL
);


ALTER TABLE public.announcements OWNER TO mysql;

--
-- Name: dinoparc_servers; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.dinoparc_servers (
    dinoparc_server public.dinoparc_server NOT NULL
);


ALTER TABLE public.dinoparc_servers OWNER TO mysql;

--
-- Name: dinoparc_sessions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.dinoparc_sessions (
    dinoparc_server public.dinoparc_server NOT NULL,
    dinoparc_session_key bytea NOT NULL,
    _dinoparc_session_key_hash bytea NOT NULL,
    dinoparc_user_id public.dinoparc_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    CONSTRAINT dinoparc_sessions_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.dinoparc_sessions OWNER TO mysql;

--
-- Name: dinoparc_user_links; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.dinoparc_user_links (
    user_id public.user_id NOT NULL,
    dinoparc_server public.dinoparc_server NOT NULL,
    dinoparc_user_id public.dinoparc_user_id NOT NULL,
    linked_at public.instant NOT NULL,
    linked_by public.user_id NOT NULL
);


ALTER TABLE public.dinoparc_user_links OWNER TO mysql;

--
-- Name: dinoparc_users; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.dinoparc_users (
    dinoparc_server public.dinoparc_server NOT NULL,
    dinoparc_user_id public.dinoparc_user_id NOT NULL,
    username public.dinoparc_username NOT NULL,
    archived_at public.instant NOT NULL
);


ALTER TABLE public.dinoparc_users OWNER TO mysql;

--
-- Name: email_verifications; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.email_verifications (
    user_id uuid NOT NULL,
    email_address bytea NOT NULL,
    ctime public.instant NOT NULL,
    validation_time public.instant,
    CONSTRAINT email_verifications_check CHECK (((validation_time)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.email_verifications OWNER TO mysql;

--
-- Name: forum_post_revisions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_post_revisions (
    forum_post_revision_id uuid NOT NULL,
    "time" public.instant,
    body text,
    _html_body text,
    mod_body text,
    _html_mod_body text,
    forum_post_id uuid NOT NULL,
    author_id uuid NOT NULL,
    comment character varying(200),
    CONSTRAINT forum_post_revisions_check CHECK (((body IS NULL) = (_html_body IS NULL))),
    CONSTRAINT forum_post_revisions_check1 CHECK (((mod_body IS NULL) = (_html_mod_body IS NULL)))
);


ALTER TABLE public.forum_post_revisions OWNER TO mysql;

--
-- Name: forum_posts; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_posts (
    forum_post_id uuid NOT NULL,
    ctime public.instant,
    forum_thread_id uuid NOT NULL
);


ALTER TABLE public.forum_posts OWNER TO mysql;

--
-- Name: forum_role_grants; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_role_grants (
    forum_section_id uuid NOT NULL,
    user_id uuid NOT NULL,
    start_time public.instant,
    granted_by uuid NOT NULL
);


ALTER TABLE public.forum_role_grants OWNER TO mysql;

--
-- Name: forum_role_revocations; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_role_revocations (
    forum_section_id uuid NOT NULL,
    user_id uuid NOT NULL,
    start_time public.instant NOT NULL,
    end_time public.instant,
    granted_by uuid NOT NULL,
    revoked_by uuid NOT NULL,
    CONSTRAINT forum_role_revocations_check CHECK (((start_time)::timestamp with time zone < (end_time)::timestamp with time zone))
);


ALTER TABLE public.forum_role_revocations OWNER TO mysql;

--
-- Name: forum_sections; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_sections (
    forum_section_id uuid NOT NULL,
    key character varying(32),
    ctime public.instant,
    display_name character varying(64) NOT NULL,
    display_name_mtime public.instant NOT NULL,
    locale character varying(10),
    locale_mtime public.instant NOT NULL,
    CONSTRAINT forum_sections_check CHECK (((display_name_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.forum_sections OWNER TO mysql;

--
-- Name: forum_threads; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.forum_threads (
    forum_thread_id uuid NOT NULL,
    key character varying(32),
    ctime public.instant,
    title character varying(64) NOT NULL,
    title_mtime public.instant NOT NULL,
    forum_section_id uuid NOT NULL,
    is_pinned boolean NOT NULL,
    is_pinned_mtime public.instant NOT NULL,
    is_locked boolean NOT NULL,
    is_locked_mtime public.instant NOT NULL,
    CONSTRAINT forum_threads_check CHECK (((title_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT forum_threads_check1 CHECK (((is_pinned_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT forum_threads_check2 CHECK (((is_locked_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.forum_threads OWNER TO mysql;

--
-- Name: hammerfest_servers; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.hammerfest_servers (
    hammerfest_server public.hammerfest_server NOT NULL,
    CONSTRAINT hammerfest_servers_domain_check CHECK (((hammerfest_server)::text = ANY (ARRAY[('hammerfest.fr'::character varying)::text, ('hfest.net'::character varying)::text, ('hammerfest.es'::character varying)::text])))
);


ALTER TABLE public.hammerfest_servers OWNER TO mysql;

--
-- Name: hammerfest_sessions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.hammerfest_sessions (
    hammerfest_server public.hammerfest_server NOT NULL,
    hammerfest_session_key bytea NOT NULL,
    _hammerfest_session_key_hash bytea NOT NULL,
    hammerfest_user_id public.hammerfest_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    CONSTRAINT hammerfest_sessions_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.hammerfest_sessions OWNER TO mysql;

--
-- Name: hammerfest_user_links; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.hammerfest_user_links (
    user_id public.user_id NOT NULL,
    hammerfest_server public.hammerfest_server NOT NULL,
    linked_at public.instant NOT NULL,
    hammerfest_user_id public.hammerfest_user_id NOT NULL,
    linked_by public.user_id NOT NULL
);


ALTER TABLE public.hammerfest_user_links OWNER TO mysql;

--
-- Name: hammerfest_users; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.hammerfest_users (
    hammerfest_server public.hammerfest_server NOT NULL,
    username character varying(20) NOT NULL,
    hammerfest_user_id public.hammerfest_user_id NOT NULL,
    archived_at public.instant NOT NULL
);


ALTER TABLE public.hammerfest_users OWNER TO mysql;

--
-- Name: oauth_access_tokens; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.oauth_access_tokens (
    oauth_access_token_id uuid NOT NULL,
    oauth_client_id uuid NOT NULL,
    user_id uuid NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    CONSTRAINT oauth_access_tokens_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.oauth_access_tokens OWNER TO mysql;

--
-- Name: oauth_clients; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.oauth_clients (
    oauth_client_id uuid NOT NULL,
    key character varying(32),
    ctime public.instant,
    display_name character varying(64) NOT NULL,
    display_name_mtime public.instant NOT NULL,
    app_uri character varying(512) NOT NULL,
    app_uri_mtime public.instant NOT NULL,
    callback_uri character varying(512) NOT NULL,
    callback_uri_mtime public.instant NOT NULL,
    secret bytea NOT NULL,
    secret_mtime public.instant NOT NULL,
    owner_id uuid,
    CONSTRAINT oauth_clients_check CHECK (((display_name_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT oauth_clients_check1 CHECK (((app_uri_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT oauth_clients_check2 CHECK (((callback_uri_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT oauth_clients_check3 CHECK (((secret_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT oauth_clients_check4 CHECK (((key IS NULL) <> (owner_id IS NULL)))
);


ALTER TABLE public.oauth_clients OWNER TO mysql;

--
-- Name: old_dinoparc_sessions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_dinoparc_sessions (
    dinoparc_server public.dinoparc_server NOT NULL,
    dinoparc_session_key bytea NOT NULL,
    _dinoparc_session_key_hash bytea NOT NULL,
    dinoparc_user_id public.dinoparc_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    dtime public.instant NOT NULL,
    CONSTRAINT old_dinoparc_sessions_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT old_dinoparc_sessions_check1 CHECK (((dtime)::timestamp with time zone >= (atime)::timestamp with time zone)),
    CONSTRAINT old_dinoparc_sessions_check2 CHECK (((dtime)::timestamp with time zone > (ctime)::timestamp with time zone))
);


ALTER TABLE public.old_dinoparc_sessions OWNER TO mysql;

--
-- Name: old_hammerfest_sessions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_hammerfest_sessions (
    hammerfest_server public.hammerfest_server NOT NULL,
    hammerfest_session_key bytea NOT NULL,
    _hammerfest_session_key_hash bytea NOT NULL,
    hammerfest_user_id public.hammerfest_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    dtime public.instant NOT NULL,
    CONSTRAINT old_hammerfest_sessions_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT old_hammerfest_sessions_check1 CHECK (((dtime)::timestamp with time zone >= (atime)::timestamp with time zone)),
    CONSTRAINT old_hammerfest_sessions_check2 CHECK (((dtime)::timestamp with time zone > (ctime)::timestamp with time zone))
);


ALTER TABLE public.old_hammerfest_sessions OWNER TO mysql;

--
-- Name: old_oauth_client_app_uris; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_oauth_client_app_uris (
    oauth_client_id uuid NOT NULL,
    start_time public.instant NOT NULL,
    app_uri character varying(512) NOT NULL
);


ALTER TABLE public.old_oauth_client_app_uris OWNER TO mysql;

--
-- Name: old_oauth_client_callback_uris; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_oauth_client_callback_uris (
    oauth_client_id uuid NOT NULL,
    start_time public.instant NOT NULL,
    callback_uri character varying(512) NOT NULL
);


ALTER TABLE public.old_oauth_client_callback_uris OWNER TO mysql;

--
-- Name: old_oauth_client_display_names; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_oauth_client_display_names (
    oauth_client_id uuid NOT NULL,
    start_time public.instant NOT NULL,
    display_name character varying(64) NOT NULL
);


ALTER TABLE public.old_oauth_client_display_names OWNER TO mysql;

--
-- Name: old_oauth_client_secrets; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_oauth_client_secrets (
    oauth_client_id uuid NOT NULL,
    start_time public.instant NOT NULL,
    secret bytea NOT NULL
);


ALTER TABLE public.old_oauth_client_secrets OWNER TO mysql;

--
-- Name: old_twinoid_access_tokens; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_twinoid_access_tokens (
    twinoid_access_token bytea NOT NULL,
    _twinoid_access_token_hash bytea NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    dtime public.instant NOT NULL,
    expiration_time public.instant NOT NULL,
    CONSTRAINT old_twinoid_access_tokens_check CHECK (((ctime)::timestamp with time zone <= (atime)::timestamp with time zone)),
    CONSTRAINT old_twinoid_access_tokens_check1 CHECK (((atime)::timestamp with time zone <= (expiration_time)::timestamp with time zone)),
    CONSTRAINT old_twinoid_access_tokens_check2 CHECK (((ctime)::timestamp with time zone < (expiration_time)::timestamp with time zone)),
    CONSTRAINT old_twinoid_access_tokens_check3 CHECK (((atime)::timestamp with time zone <= (dtime)::timestamp with time zone)),
    CONSTRAINT old_twinoid_access_tokens_check4 CHECK (((ctime)::timestamp with time zone < (dtime)::timestamp with time zone))
);


ALTER TABLE public.old_twinoid_access_tokens OWNER TO mysql;

--
-- Name: old_twinoid_refresh_tokens; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_twinoid_refresh_tokens (
    twinoid_refresh_token bytea NOT NULL,
    _twinoid_refresh_token_hash bytea NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    dtime public.instant NOT NULL,
    CONSTRAINT old_twinoid_refresh_tokens_check CHECK (((ctime)::timestamp with time zone <= (atime)::timestamp with time zone)),
    CONSTRAINT old_twinoid_refresh_tokens_check1 CHECK (((atime)::timestamp with time zone <= (dtime)::timestamp with time zone)),
    CONSTRAINT old_twinoid_refresh_tokens_check2 CHECK (((ctime)::timestamp with time zone < (dtime)::timestamp with time zone))
);


ALTER TABLE public.old_twinoid_refresh_tokens OWNER TO mysql;

--
-- Name: old_twinoid_user_links; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.old_twinoid_user_links (
    user_id uuid NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    start_time public.instant,
    end_time public.instant
);


ALTER TABLE public.old_twinoid_user_links OWNER TO mysql;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.sessions (
    session_id uuid NOT NULL,
    user_id uuid,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    data json NOT NULL,
    CONSTRAINT sessions_check CHECK (((atime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.sessions OWNER TO mysql;

--
-- Name: twinoid_access_tokens; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.twinoid_access_tokens (
    twinoid_access_token bytea NOT NULL,
    _twinoid_access_token_hash bytea NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    expiration_time public.instant NOT NULL,
    CONSTRAINT twinoid_access_tokens_check CHECK (((ctime)::timestamp with time zone <= (atime)::timestamp with time zone)),
    CONSTRAINT twinoid_access_tokens_check1 CHECK (((atime)::timestamp with time zone <= (expiration_time)::timestamp with time zone)),
    CONSTRAINT twinoid_access_tokens_check2 CHECK (((ctime)::timestamp with time zone < (expiration_time)::timestamp with time zone))
);


ALTER TABLE public.twinoid_access_tokens OWNER TO mysql;

--
-- Name: twinoid_refresh_tokens; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.twinoid_refresh_tokens (
    twinoid_refresh_token bytea NOT NULL,
    _twinoid_refresh_token_hash bytea NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    ctime public.instant NOT NULL,
    atime public.instant NOT NULL,
    CONSTRAINT twinoid_refresh_tokens_check CHECK (((ctime)::timestamp with time zone <= (atime)::timestamp with time zone))
);


ALTER TABLE public.twinoid_refresh_tokens OWNER TO mysql;

--
-- Name: twinoid_user_links; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.twinoid_user_links (
    user_id public.user_id NOT NULL,
    twinoid_user_id public.twinoid_user_id NOT NULL,
    linked_at public.instant NOT NULL,
    linked_by public.user_id NOT NULL
);


ALTER TABLE public.twinoid_user_links OWNER TO mysql;

--
-- Name: twinoid_users; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.twinoid_users (
    twinoid_user_id public.twinoid_user_id NOT NULL,
    name character varying(50) NOT NULL,
    archived_at public.instant NOT NULL
);


ALTER TABLE public.twinoid_users OWNER TO mysql;

--
-- Name: users; Type: TABLE; Schema: public; Owner: mysql
--

CREATE TABLE public.users (
    user_id uuid NOT NULL,
    ctime public.instant,
    display_name character varying(64) NOT NULL,
    display_name_mtime public.instant NOT NULL,
    email_address bytea,
    email_address_mtime public.instant NOT NULL,
    username character varying(64),
    username_mtime public.instant NOT NULL,
    password bytea,
    password_mtime public.instant NOT NULL,
    is_administrator boolean NOT NULL,
    CONSTRAINT users_check CHECK (((display_name_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT users_check1 CHECK (((email_address_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT users_check2 CHECK (((username_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone)),
    CONSTRAINT users_check3 CHECK (((password_mtime)::timestamp with time zone >= (ctime)::timestamp with time zone))
);


ALTER TABLE public.users OWNER TO mysql;

--
-- Data for Name: _post_formatting_costs; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public._post_formatting_costs (forum_post_revision_id, formatting, cost) FROM stdin;
\.


--
-- Data for Name: announcements; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.announcements (announcement_id, forum_thread_id, locale, created_at, created_by) FROM stdin;
\.


--
-- Data for Name: dinoparc_servers; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.dinoparc_servers (dinoparc_server) FROM stdin;
dinoparc.com
en.dinoparc.com
sp.dinoparc.com
\.


--
-- Data for Name: dinoparc_sessions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.dinoparc_sessions (dinoparc_server, dinoparc_session_key, _dinoparc_session_key_hash, dinoparc_user_id, ctime, atime) FROM stdin;
\.


--
-- Data for Name: dinoparc_user_links; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.dinoparc_user_links (user_id, dinoparc_server, dinoparc_user_id, linked_at, linked_by) FROM stdin;
\.


--
-- Data for Name: dinoparc_users; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.dinoparc_users (dinoparc_server, dinoparc_user_id, username, archived_at) FROM stdin;
\.


--
-- Data for Name: email_verifications; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.email_verifications (user_id, email_address, ctime, validation_time) FROM stdin;
\.


--
-- Data for Name: forum_post_revisions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_post_revisions (forum_post_revision_id, "time", body, _html_body, mod_body, _html_mod_body, forum_post_id, author_id, comment) FROM stdin;
\.


--
-- Data for Name: forum_posts; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_posts (forum_post_id, ctime, forum_thread_id) FROM stdin;
\.


--
-- Data for Name: forum_role_grants; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_role_grants (forum_section_id, user_id, start_time, granted_by) FROM stdin;
\.


--
-- Data for Name: forum_role_revocations; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_role_revocations (forum_section_id, user_id, start_time, end_time, granted_by, revoked_by) FROM stdin;
\.


--
-- Data for Name: forum_sections; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_sections (forum_section_id, key, ctime, display_name, display_name_mtime, locale, locale_mtime) FROM stdin;
5c71de40-9d46-4ea4-b4b5-f964e5872d6e	en_main	2021-01-21 05:24:13.072+00	Main Forum (en-US)	2021-01-21 05:24:13.072+00	en-US	2021-01-21 05:24:13.072+00
1ee77821-94f1-45be-ab16-17aae08539f6	fr_main	2021-01-21 05:24:13.076+00	Forum Général (fr-FR)	2021-01-21 05:24:13.076+00	fr-FR	2021-01-21 05:24:13.076+00
053e7ea4-339a-4c7e-8f6d-ef9983e99021	es_main	2021-01-21 05:24:13.078+00	Foro principal (es-SP)	2021-01-21 05:24:13.078+00	es-SP	2021-01-21 05:24:13.078+00
e591b4ad-a491-4af3-8287-8ce208e19326	eo_main	2021-01-21 05:24:13.08+00	Ĉefa forumo (eo)	2021-01-21 05:24:13.08+00	eo	2021-01-21 05:24:13.08+00
4f50b9c5-4e7d-4c9e-b1c3-cba934966fa0	eternalfest_main	2021-01-21 05:24:13.083+00	[Eternalfest] Le Panthéon	2021-01-21 05:24:13.083+00	fr-FR	2021-01-21 05:24:13.083+00
338f9a9b-1081-4c9f-bdd4-dd686ea92b87	emush_main	2021-01-21 05:24:13.085+00	[Emush] Neron is watching you	2021-01-21 05:24:13.085+00	fr-FR	2021-01-21 05:24:13.085+00
92158cf1-4bfb-4817-a248-8d76ed1d3897	drpg_main	2021-01-21 05:24:13.087+00	[DinoRPG] Jurassic Park	2021-01-21 05:24:13.087+00	fr-FR	2021-01-21 05:24:13.087+00
38dad202-f912-4c76-bf57-92812ad621b2	myhordes_main	2021-01-21 05:24:13.09+00	[Myhordes] Le Saloon	2021-01-21 05:24:13.09+00	fr-FR	2021-01-21 05:24:13.09+00
26c99a17-57e2-4aff-a8db-cb4b295835ac	kadokadeo_main	2021-01-21 05:24:13.092+00	[Kadokadeo] Café des palabres	2021-01-21 05:24:13.092+00	fr-FR	2021-01-21 05:24:13.092+00
9c24c224-fc60-4445-9c74-cfec5c9607f5	kingdom_main	2021-01-21 05:24:13.094+00	[Kingdom] La foire du trône	2021-01-21 05:24:13.094+00	fr-FR	2021-01-21 05:24:13.094+00
9ff93f57-6dc9-4b1a-ad3d-af9d1f9e2ff3	mjrt_main	2021-01-21 05:24:13.097+00	[Mjrt] La bergerie	2021-01-21 05:24:13.097+00	fr-FR	2021-01-21 05:24:13.097+00
b920ccd0-cec2-4de3-be44-5e95b260548e	na_main	2021-01-21 05:24:13.1+00	[Naturalchimie] Le laboratoire	2021-01-21 05:24:13.1+00	fr-FR	2021-01-21 05:24:13.1+00
8a7556a0-46d0-415d-bb9d-42e29cee073b	sq_main	2021-01-21 05:24:13.102+00	[Studioquiz] Le bar à questions	2021-01-21 05:24:13.102+00	fr-FR	2021-01-21 05:24:13.102+00
5fcb2326-8988-42da-82c4-06fc9bd0557e	ts_main	2021-01-21 05:24:13.105+00	[Teacher Story] La salle des profs	2021-01-21 05:24:13.105+00	fr-FR	2021-01-21 05:24:13.105+00
8f86ec83-e8b7-42e1-b0ac-5b06de36497e	ts_popotamo	2021-01-21 05:24:13.108+00	[Popotamo] Le mot le plus long	2021-01-21 05:24:13.108+00	fr-FR	2021-01-21 05:24:13.108+00
\.


--
-- Data for Name: forum_threads; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.forum_threads (forum_thread_id, key, ctime, title, title_mtime, forum_section_id, is_pinned, is_pinned_mtime, is_locked, is_locked_mtime) FROM stdin;
\.


--
-- Data for Name: hammerfest_servers; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.hammerfest_servers (hammerfest_server) FROM stdin;
hammerfest.fr
hfest.net
hammerfest.es
\.


--
-- Data for Name: hammerfest_sessions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.hammerfest_sessions (hammerfest_server, hammerfest_session_key, _hammerfest_session_key_hash, hammerfest_user_id, ctime, atime) FROM stdin;
\.


--
-- Data for Name: hammerfest_user_links; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.hammerfest_user_links (user_id, hammerfest_server, linked_at, hammerfest_user_id, linked_by) FROM stdin;
\.


--
-- Data for Name: hammerfest_users; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.hammerfest_users (hammerfest_server, username, hammerfest_user_id, archived_at) FROM stdin;
\.


--
-- Data for Name: oauth_access_tokens; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.oauth_access_tokens (oauth_access_token_id, oauth_client_id, user_id, ctime, atime) FROM stdin;
\.


--
-- Data for Name: oauth_clients; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.oauth_clients (oauth_client_id, key, ctime, display_name, display_name_mtime, app_uri, app_uri_mtime, callback_uri, callback_uri_mtime, secret, secret_mtime, owner_id) FROM stdin;
88715211-d943-45a1-9257-ef7a9a8c97e9	eternalfest@clients	2021-01-21 05:24:11.455+00	Eternalfest	2021-01-21 05:24:11.455+00	http://localhost:50313/	2021-01-21 05:24:11.455+00	http://localhost:50313/oauth/callback	2021-01-21 05:24:11.455+00	\\xc30d04070302447b286afdb3368274d291010e9617f9a8058c95785fc9395ee95316cdf221222389b40319d8c0f1c0ed216b2fde2193ab10b4c4b17f0c319fff55ca94a794d2843b3ff2e6bd1c6916fe1e744791ada76a4f10fbec29e4e97120f7127e039acca832820e43e893b2f4db3cf2652a3f894059f99492312c9fbc299ff620e627771447244be1a2fae022020abea909a2a479521abe64e1258c0d35ea5a	2021-01-21 05:24:11.455+00	\N
db9102a0-60f3-4c2a-9843-e7cc05853812	myhordes@clients	2021-01-21 05:24:11.732+00	MyHordes	2021-01-21 05:24:11.732+00	http://myhordes.localhost/	2021-01-21 05:24:11.732+00	http://myhordes.localhost/oauth/callback	2021-01-21 05:24:11.732+00	\\xc30d040703028ac8549c00eee0f867d29101166320138c727dfa7f8ee38fb1ecd1fbbec3880812b0873d83543fc75dc7043bf21365f110c807ab1f3fd056e349273799ce3ab031f49b671e295d552d58b46d7b8f2a9581c3e0d720e734e86ff0d051f8e5b39cc0c5ccc509a1972156cd868bf4fa3a7e4561975ff1fe61873de758cf446f9af5ad5a517e078b63b8497a20c88206fff7343d2dd48324967f525c7028	2021-01-21 05:24:11.732+00	\N
f538f8fe-9568-4037-b4a4-2a54737e84e1	neoparc@clients	2021-01-21 05:24:11.997+00	NeoParc	2021-01-21 05:24:11.997+00	http://neoparc.localhost/	2021-01-21 05:24:11.997+00	http://neoparc.localhost/oauth/callback	2021-01-21 05:24:11.997+00	\\xc30d04070302f71d67aaf08e0f0962d29101b79d3761adca04c3c16e4ff3fed6c4a9daca3fd8b3037be06691b57b69c506dcefe2cbca07e7e80640cf07f875f6152972c8b067b289211fd3e5a598bbccfa8ef3dcc81bd69bc6a76c6134564c714e4487e0f761dc510f9fd63c254b35b6986d7e50c473ba61cfa828b102668bb9471a3c542f8b89be88898cd930d94544ea047703b0d3a137bfe8b6012a4bb5bb683d	2021-01-21 05:24:11.997+00	\N
3554c563-6169-453a-9019-38ba1e2b505a	kadokadeo@clients	2021-01-21 05:24:12.283+00	Kadokadeo	2021-01-21 05:24:12.283+00	http://kadokadeo.localhost/	2021-01-21 05:24:12.283+00	http://kadokadeo.localhost/oauth/callback	2021-01-21 05:24:12.283+00	\\xc30d040703023a1dd50276ce667376d29101e57503736e3cdc0a04a03716151b035bdcbe56c202dd7cabf36ee9901994ebca73b45aa5bec88b1e00f782408786aaa3a782bf1947fc7ce4b697a72dff9397596668a98deee3517d850036222f0d2c44a1d435093a239c24b37da762ab8367d66cc25d50a2110d8b0a634249325060339bac9f4ee42829fb121a750df0a31fa482cbf46739cdc9f6cd3ebb219edfff82	2021-01-21 05:24:12.283+00	\N
1e18d5c2-5822-4b51-871a-0b310c0e6ffa	mjrt@clients	2021-01-21 05:24:12.548+00	Mjrt	2021-01-21 05:24:12.548+00	http://mjrt.localhost/	2021-01-21 05:24:12.548+00	http://mjrt.localhost/oauth/callback	2021-01-21 05:24:12.548+00	\\xc30d04070302f04a32f70cf740ea67d291018ae6c1d2bc47a3e7ba58309c7af27467da12f067b65739420bcac252b7225583403e3e5897e728415a7824d6b5384fd8dedf5d212427925c430809d9f218854cbcdbe186a0b85a4edb3ced33a925372d6e54419fed909de20e892ce1c115aa54415984456a51c03e2ff077530e30402ae573097321e68cd719ae806357528afc0729e8a6f2e3d65e34cf4516b6cc8574	2021-01-21 05:24:12.548+00	\N
c7095a45-9122-4218-9abd-9eb839e97202	na3@clients	2021-01-21 05:24:12.81+00	Naturalchimie3	2021-01-21 05:24:12.81+00	http://localhost:8080/	2021-01-21 05:24:12.81+00	http://localhost:8080/oauth/callback	2021-01-21 05:24:12.81+00	\\xc30d04070302d3d2492a36c9d72d64d29101c125946644e1aded5b1c1d8ad5154573e8fddff4309f45b5ce4793a58632802b9eb06bf4f62acb869717f85a40c3a7fbbdd597dc5e9a8bc1587172526dcb353256cebb582fd02adfecf6eab0864e68414fbfff000a7260fc8d3494e3bd585ba34a0f602d9907947bb57f54a18b3a11b71f66ae234527972e7c1c094f53944f3bb6f2c1bc6d102f20dbcd16e536536d94	2021-01-21 05:24:12.81+00	\N
\.


--
-- Data for Name: old_dinoparc_sessions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_dinoparc_sessions (dinoparc_server, dinoparc_session_key, _dinoparc_session_key_hash, dinoparc_user_id, ctime, atime, dtime) FROM stdin;
\.


--
-- Data for Name: old_hammerfest_sessions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_hammerfest_sessions (hammerfest_server, hammerfest_session_key, _hammerfest_session_key_hash, hammerfest_user_id, ctime, atime, dtime) FROM stdin;
\.


--
-- Data for Name: old_oauth_client_app_uris; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_oauth_client_app_uris (oauth_client_id, start_time, app_uri) FROM stdin;
\.


--
-- Data for Name: old_oauth_client_callback_uris; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_oauth_client_callback_uris (oauth_client_id, start_time, callback_uri) FROM stdin;
\.


--
-- Data for Name: old_oauth_client_display_names; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_oauth_client_display_names (oauth_client_id, start_time, display_name) FROM stdin;
\.


--
-- Data for Name: old_oauth_client_secrets; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_oauth_client_secrets (oauth_client_id, start_time, secret) FROM stdin;
\.


--
-- Data for Name: old_twinoid_access_tokens; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_twinoid_access_tokens (twinoid_access_token, _twinoid_access_token_hash, twinoid_user_id, ctime, atime, dtime, expiration_time) FROM stdin;
\.


--
-- Data for Name: old_twinoid_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_twinoid_refresh_tokens (twinoid_refresh_token, _twinoid_refresh_token_hash, twinoid_user_id, ctime, atime, dtime) FROM stdin;
\.


--
-- Data for Name: old_twinoid_user_links; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.old_twinoid_user_links (user_id, twinoid_user_id, start_time, end_time) FROM stdin;
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.sessions (session_id, user_id, ctime, atime, data) FROM stdin;
271f0a28-a533-42d0-9368-41751c5b2b67	fade685f-adc5-4f71-840f-1fbf36e62228	2021-01-21 05:25:51.525+00	2021-01-21 05:25:51.525+00	{}
\.


--
-- Data for Name: twinoid_access_tokens; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.twinoid_access_tokens (twinoid_access_token, _twinoid_access_token_hash, twinoid_user_id, ctime, atime, expiration_time) FROM stdin;
\.


--
-- Data for Name: twinoid_refresh_tokens; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.twinoid_refresh_tokens (twinoid_refresh_token, _twinoid_refresh_token_hash, twinoid_user_id, ctime, atime) FROM stdin;
\.


--
-- Data for Name: twinoid_user_links; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.twinoid_user_links (user_id, twinoid_user_id, linked_at, linked_by) FROM stdin;
\.


--
-- Data for Name: twinoid_users; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.twinoid_users (twinoid_user_id, name, archived_at) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: mysql
--

COPY public.users (user_id, ctime, display_name, display_name_mtime, email_address, email_address_mtime, username, username_mtime, password, password_mtime, is_administrator) FROM stdin;
fade685f-adc5-4f71-840f-1fbf36e62228	2021-01-21 05:25:51.52+00	DocteurDisplay	2021-01-21 05:25:51.52+00	\N	2021-01-21 05:25:51.52+00	docteur	2021-01-21 05:25:51.52+00	\\xc30d04070302cfd015f3fdbdef3566d2910190b52d3f2ff8fee2b5bbef6ede4db787c64e42a2f223f7dda8b63b4de5db17dc76fa514db970759fe7f0d8ecc8af72d111acec77bb219c4410772e6092221eb566dc6e85664c1bd12f6bd4bda69484eb4e845718104ab7a5476281245e973a5d02fb36a2b850212406e873d7b4f4bbd1d1c9848a678f2f25abc2b401c7c142855372fdbbc4a217f991b875ff051c3cbd	2021-01-21 05:25:51.525+00	t
\.


--
-- Name: _post_formatting_costs _post_formatting_costs_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public._post_formatting_costs
    ADD CONSTRAINT _post_formatting_costs_pkey PRIMARY KEY (forum_post_revision_id, formatting);


--
-- Name: announcements announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (announcement_id);


--
-- Name: dinoparc_servers dinoparc_servers_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_servers
    ADD CONSTRAINT dinoparc_servers_pkey PRIMARY KEY (dinoparc_server);


--
-- Name: dinoparc_sessions dinoparc_sessions_dinoparc_server_dinoparc_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_sessions
    ADD CONSTRAINT dinoparc_sessions_dinoparc_server_dinoparc_user_id_key UNIQUE (dinoparc_server, dinoparc_user_id);


--
-- Name: dinoparc_sessions dinoparc_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_sessions
    ADD CONSTRAINT dinoparc_sessions_pkey PRIMARY KEY (dinoparc_server, _dinoparc_session_key_hash);


--
-- Name: dinoparc_user_links dinoparc_user_links_dinoparc_server_dinoparc_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_links_dinoparc_server_dinoparc_user_id_key UNIQUE (dinoparc_server, dinoparc_user_id);


--
-- Name: dinoparc_user_links dinoparc_user_links_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_links_pkey PRIMARY KEY (user_id, dinoparc_server, dinoparc_user_id);


--
-- Name: dinoparc_user_links dinoparc_user_links_user_id_dinoparc_server_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_links_user_id_dinoparc_server_key UNIQUE (user_id, dinoparc_server);


--
-- Name: dinoparc_users dinoparc_users_dinoparc_server_username_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_users
    ADD CONSTRAINT dinoparc_users_dinoparc_server_username_key UNIQUE (dinoparc_server, username);


--
-- Name: dinoparc_users dinoparc_users_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_users
    ADD CONSTRAINT dinoparc_users_pkey PRIMARY KEY (dinoparc_server, dinoparc_user_id);


--
-- Name: forum_post_revisions forum_post_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_post_revisions
    ADD CONSTRAINT forum_post_revisions_pkey PRIMARY KEY (forum_post_revision_id);


--
-- Name: forum_posts forum_posts_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_posts
    ADD CONSTRAINT forum_posts_pkey PRIMARY KEY (forum_post_id);


--
-- Name: forum_role_grants forum_role_grants_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_grants
    ADD CONSTRAINT forum_role_grants_pkey PRIMARY KEY (forum_section_id, user_id);


--
-- Name: forum_role_revocations forum_role_revocations_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_revocations
    ADD CONSTRAINT forum_role_revocations_pkey PRIMARY KEY (forum_section_id, user_id, start_time);


--
-- Name: forum_sections forum_sections_key_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_sections
    ADD CONSTRAINT forum_sections_key_key UNIQUE (key);


--
-- Name: forum_sections forum_sections_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_sections
    ADD CONSTRAINT forum_sections_pkey PRIMARY KEY (forum_section_id);


--
-- Name: forum_threads forum_threads_key_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_threads
    ADD CONSTRAINT forum_threads_key_key UNIQUE (key);


--
-- Name: forum_threads forum_threads_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_threads
    ADD CONSTRAINT forum_threads_pkey PRIMARY KEY (forum_thread_id);


--
-- Name: hammerfest_servers hammerfest_servers_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_servers
    ADD CONSTRAINT hammerfest_servers_pkey PRIMARY KEY (hammerfest_server);


--
-- Name: hammerfest_sessions hammerfest_sessions_hammerfest_server_hammerfest_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_sessions
    ADD CONSTRAINT hammerfest_sessions_hammerfest_server_hammerfest_user_id_key UNIQUE (hammerfest_server, hammerfest_user_id);


--
-- Name: hammerfest_sessions hammerfest_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_sessions
    ADD CONSTRAINT hammerfest_sessions_pkey PRIMARY KEY (hammerfest_server, _hammerfest_session_key_hash);


--
-- Name: hammerfest_user_links hammerfest_user_links_hammerfest_server_hammerfest_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_links_hammerfest_server_hammerfest_user_id_key UNIQUE (hammerfest_server, hammerfest_user_id);


--
-- Name: hammerfest_user_links hammerfest_user_links_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_links_pkey PRIMARY KEY (user_id, hammerfest_server, hammerfest_user_id);


--
-- Name: hammerfest_user_links hammerfest_user_links_user_id_hammerfest_server_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_links_user_id_hammerfest_server_key UNIQUE (user_id, hammerfest_server);


--
-- Name: hammerfest_users hammerfest_users_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_users
    ADD CONSTRAINT hammerfest_users_pkey PRIMARY KEY (hammerfest_server, hammerfest_user_id);


--
-- Name: hammerfest_users hammerfest_users_server_username_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_users
    ADD CONSTRAINT hammerfest_users_server_username_key UNIQUE (hammerfest_server, username);


--
-- Name: oauth_access_tokens oauth_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_tokens_pkey PRIMARY KEY (oauth_access_token_id);


--
-- Name: oauth_clients oauth_clients_key_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_key_key UNIQUE (key);


--
-- Name: oauth_clients oauth_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.oauth_clients
    ADD CONSTRAINT oauth_clients_pkey PRIMARY KEY (oauth_client_id);


--
-- Name: old_dinoparc_sessions old_dinoparc_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_dinoparc_sessions
    ADD CONSTRAINT old_dinoparc_sessions_pkey PRIMARY KEY (dinoparc_server, _dinoparc_session_key_hash, ctime);


--
-- Name: old_hammerfest_sessions old_hammerfest_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_hammerfest_sessions
    ADD CONSTRAINT old_hammerfest_sessions_pkey PRIMARY KEY (hammerfest_server, _hammerfest_session_key_hash, ctime);


--
-- Name: old_oauth_client_app_uris old_oauth_client_app_uris_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_app_uris
    ADD CONSTRAINT old_oauth_client_app_uris_pkey PRIMARY KEY (oauth_client_id, start_time);


--
-- Name: old_oauth_client_callback_uris old_oauth_client_callback_uris_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_callback_uris
    ADD CONSTRAINT old_oauth_client_callback_uris_pkey PRIMARY KEY (oauth_client_id, start_time);


--
-- Name: old_oauth_client_display_names old_oauth_client_display_names_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_display_names
    ADD CONSTRAINT old_oauth_client_display_names_pkey PRIMARY KEY (oauth_client_id, start_time);


--
-- Name: old_oauth_client_secrets old_oauth_client_secrets_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_secrets
    ADD CONSTRAINT old_oauth_client_secrets_pkey PRIMARY KEY (oauth_client_id, start_time);


--
-- Name: old_twinoid_access_tokens old_twinoid_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_access_tokens
    ADD CONSTRAINT old_twinoid_access_tokens_pkey PRIMARY KEY (_twinoid_access_token_hash, ctime);


--
-- Name: old_twinoid_refresh_tokens old_twinoid_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_refresh_tokens
    ADD CONSTRAINT old_twinoid_refresh_tokens_pkey PRIMARY KEY (_twinoid_refresh_token_hash, ctime);


--
-- Name: old_twinoid_user_links old_twinoid_user_links_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_user_links
    ADD CONSTRAINT old_twinoid_user_links_pkey PRIMARY KEY (user_id, twinoid_user_id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (session_id);


--
-- Name: twinoid_access_tokens twinoid_access_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_access_tokens
    ADD CONSTRAINT twinoid_access_tokens_pkey PRIMARY KEY (_twinoid_access_token_hash);


--
-- Name: twinoid_access_tokens twinoid_access_tokens_twinoid_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_access_tokens
    ADD CONSTRAINT twinoid_access_tokens_twinoid_user_id_key UNIQUE (twinoid_user_id);


--
-- Name: twinoid_refresh_tokens twinoid_refresh_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_refresh_tokens
    ADD CONSTRAINT twinoid_refresh_tokens_pkey PRIMARY KEY (_twinoid_refresh_token_hash);


--
-- Name: twinoid_refresh_tokens twinoid_refresh_tokens_twinoid_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_refresh_tokens
    ADD CONSTRAINT twinoid_refresh_tokens_twinoid_user_id_key UNIQUE (twinoid_user_id);


--
-- Name: twinoid_user_links twinoid_user_links_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_links_pkey PRIMARY KEY (user_id, twinoid_user_id);


--
-- Name: twinoid_user_links twinoid_user_links_twinoid_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_links_twinoid_user_id_key UNIQUE (twinoid_user_id);


--
-- Name: twinoid_user_links twinoid_user_links_user_id_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_links_user_id_key UNIQUE (user_id);


--
-- Name: twinoid_users twinoid_users_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_users
    ADD CONSTRAINT twinoid_users_pkey PRIMARY KEY (twinoid_user_id);


--
-- Name: users username__uniq; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT username__uniq UNIQUE (username);


--
-- Name: users users_email_address_key; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_address_key UNIQUE (email_address);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: announcements announcement__forum_thread__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcement__forum_thread__fk FOREIGN KEY (forum_thread_id) REFERENCES public.forum_threads(forum_thread_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: announcements announcement__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcement__user__fk FOREIGN KEY (created_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dinoparc_sessions dinoparc_session__dinoparc_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_sessions
    ADD CONSTRAINT dinoparc_session__dinoparc_user__fk FOREIGN KEY (dinoparc_server, dinoparc_user_id) REFERENCES public.dinoparc_users(dinoparc_server, dinoparc_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dinoparc_users dinoparc_user__dinoparc_server__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_users
    ADD CONSTRAINT dinoparc_user__dinoparc_server__fk FOREIGN KEY (dinoparc_server) REFERENCES public.dinoparc_servers(dinoparc_server) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: dinoparc_user_links dinoparc_user_link__dinoparc_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_link__dinoparc_user__fk FOREIGN KEY (dinoparc_server, dinoparc_user_id) REFERENCES public.dinoparc_users(dinoparc_server, dinoparc_user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: dinoparc_user_links dinoparc_user_link__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_link__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: dinoparc_user_links dinoparc_user_link_linked_by__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.dinoparc_user_links
    ADD CONSTRAINT dinoparc_user_link_linked_by__user__fk FOREIGN KEY (linked_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: email_verifications email_verification__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.email_verifications
    ADD CONSTRAINT email_verification__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_role_grants forum_moderator__forum_section__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_grants
    ADD CONSTRAINT forum_moderator__forum_section__fk FOREIGN KEY (forum_section_id) REFERENCES public.forum_sections(forum_section_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_role_grants forum_moderator__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_grants
    ADD CONSTRAINT forum_moderator__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_role_grants forum_moderator_granter__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_grants
    ADD CONSTRAINT forum_moderator_granter__user__fk FOREIGN KEY (granted_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: forum_role_revocations forum_moderator_granter__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_revocations
    ADD CONSTRAINT forum_moderator_granter__user__fk FOREIGN KEY (granted_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: forum_role_revocations forum_moderator_revoker__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_revocations
    ADD CONSTRAINT forum_moderator_revoker__user__fk FOREIGN KEY (revoked_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: forum_posts forum_post__forum_thread__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_posts
    ADD CONSTRAINT forum_post__forum_thread__fk FOREIGN KEY (forum_thread_id) REFERENCES public.forum_threads(forum_thread_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_post_revisions forum_post_revision__forum_revision__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_post_revisions
    ADD CONSTRAINT forum_post_revision__forum_revision__fk FOREIGN KEY (forum_post_id) REFERENCES public.forum_posts(forum_post_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: _post_formatting_costs forum_post_revision__forum_revision__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public._post_formatting_costs
    ADD CONSTRAINT forum_post_revision__forum_revision__fk FOREIGN KEY (forum_post_revision_id) REFERENCES public.forum_post_revisions(forum_post_revision_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_post_revisions forum_post_revision__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_post_revisions
    ADD CONSTRAINT forum_post_revision__user__fk FOREIGN KEY (author_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: forum_role_revocations forum_role_revocation__forum_section__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_revocations
    ADD CONSTRAINT forum_role_revocation__forum_section__fk FOREIGN KEY (forum_section_id) REFERENCES public.forum_sections(forum_section_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_role_revocations forum_role_revocation_user__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_role_revocations
    ADD CONSTRAINT forum_role_revocation_user__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: forum_threads forum_thread__forum_section__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.forum_threads
    ADD CONSTRAINT forum_thread__forum_section__fk FOREIGN KEY (forum_section_id) REFERENCES public.forum_sections(forum_section_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hammerfest_sessions hammerfest_session__hammerfest_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_sessions
    ADD CONSTRAINT hammerfest_session__hammerfest_user__fk FOREIGN KEY (hammerfest_server, hammerfest_user_id) REFERENCES public.hammerfest_users(hammerfest_server, hammerfest_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: hammerfest_users hammerfest_user__hammerfest_server__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_users
    ADD CONSTRAINT hammerfest_user__hammerfest_server__fk FOREIGN KEY (hammerfest_server) REFERENCES public.hammerfest_servers(hammerfest_server) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: hammerfest_user_links hammerfest_user_link__hammerfest_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_link__hammerfest_user__fk FOREIGN KEY (hammerfest_server, hammerfest_user_id) REFERENCES public.hammerfest_users(hammerfest_server, hammerfest_user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: hammerfest_user_links hammerfest_user_link__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_link__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: hammerfest_user_links hammerfest_user_link_linked_by__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.hammerfest_user_links
    ADD CONSTRAINT hammerfest_user_link_linked_by__user__fk FOREIGN KEY (linked_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: oauth_access_tokens oauth_access_token__oauth_client__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_token__oauth_client__fk FOREIGN KEY (oauth_client_id) REFERENCES public.oauth_clients(oauth_client_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: oauth_access_tokens oauth_access_token__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.oauth_access_tokens
    ADD CONSTRAINT oauth_access_token__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_dinoparc_sessions old_dinoparc_session__dinoparc_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_dinoparc_sessions
    ADD CONSTRAINT old_dinoparc_session__dinoparc_user__fk FOREIGN KEY (dinoparc_server, dinoparc_user_id) REFERENCES public.dinoparc_users(dinoparc_server, dinoparc_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_hammerfest_sessions old_hammerfest_session__hammerfest_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_hammerfest_sessions
    ADD CONSTRAINT old_hammerfest_session__hammerfest_user__fk FOREIGN KEY (hammerfest_server, hammerfest_user_id) REFERENCES public.hammerfest_users(hammerfest_server, hammerfest_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_oauth_client_app_uris old_oauth_client_app_uri__oauth_client__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_app_uris
    ADD CONSTRAINT old_oauth_client_app_uri__oauth_client__fk FOREIGN KEY (oauth_client_id) REFERENCES public.oauth_clients(oauth_client_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_oauth_client_callback_uris old_oauth_client_callback_uri__oauth_client__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_callback_uris
    ADD CONSTRAINT old_oauth_client_callback_uri__oauth_client__fk FOREIGN KEY (oauth_client_id) REFERENCES public.oauth_clients(oauth_client_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_oauth_client_display_names old_oauth_client_display_name__oauth_client__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_display_names
    ADD CONSTRAINT old_oauth_client_display_name__oauth_client__fk FOREIGN KEY (oauth_client_id) REFERENCES public.oauth_clients(oauth_client_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_oauth_client_secrets old_oauth_client_secret__oauth_client__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_oauth_client_secrets
    ADD CONSTRAINT old_oauth_client_secret__oauth_client__fk FOREIGN KEY (oauth_client_id) REFERENCES public.oauth_clients(oauth_client_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_twinoid_refresh_tokens old_twinoid_refresh_token__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_refresh_tokens
    ADD CONSTRAINT old_twinoid_refresh_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: sessions session__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT session__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: twinoid_access_tokens twinoid_access_token__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_access_tokens
    ADD CONSTRAINT twinoid_access_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: old_twinoid_access_tokens twinoid_access_token__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_access_tokens
    ADD CONSTRAINT twinoid_access_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: twinoid_refresh_tokens twinoid_refresh_token__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_refresh_tokens
    ADD CONSTRAINT twinoid_refresh_token__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: twinoid_user_links twinoid_user_link__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_link__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: old_twinoid_user_links twinoid_user_link__twinoid_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_user_links
    ADD CONSTRAINT twinoid_user_link__twinoid_user__fk FOREIGN KEY (twinoid_user_id) REFERENCES public.twinoid_users(twinoid_user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: old_twinoid_user_links twinoid_user_link__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.old_twinoid_user_links
    ADD CONSTRAINT twinoid_user_link__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: twinoid_user_links twinoid_user_link__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_link__user__fk FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: twinoid_user_links twinoid_user_link_linked_by__user__fk; Type: FK CONSTRAINT; Schema: public; Owner: mysql
--

ALTER TABLE ONLY public.twinoid_user_links
    ADD CONSTRAINT twinoid_user_link_linked_by__user__fk FOREIGN KEY (linked_by) REFERENCES public.users(user_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

