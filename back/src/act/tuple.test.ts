import * as tuple from './tuple'
import { Location } from '../lib/Location'

describe('Generate elemental tuple', () => {
  test('We should get a coherent distribution', () => {
    const result : number[] = tuple.generateElementalTuple(Location.getLocationIdByName('Donjon K'), 11, 1000000).map((e) => typeof e !== 'number' ? parseInt(e) : e)
    const count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let i = 0; i < result.length; i++) {
      count[result[i]] += 1
    }
    // There should be more potions than menthols
    expect(count[0]).toBeGreaterThan(count[4])
    expect(count[1]).toBeGreaterThan(count[4])
    expect(count[2]).toBeGreaterThan(count[4])
    expect(count[3]).toBeGreaterThan(count[4])

    // More menthols than eyes
    expect(count[4]).toBeGreaterThan(count[5])

    // More eyes than skulls
    expect(count[5]).toBeGreaterThan(count[6])

    // More skulls than flowers
    expect(count[6]).toBeGreaterThan(count[7])

    // More flowers than oxydes
    expect(count[7]).toBeGreaterThan(count[8])

    // And more oxydes than coppers or quicksilvers
    expect(count[8]).toBeGreaterThan(count[9])
    expect(count[8]).toBeGreaterThan(count[10])

    // There should be 0 nuggets
    expect(count[11]).toBe(0)

    count[0] /= 180000 / 1.1
    count[1] /= 180000 / 1.1
    count[2] /= 180000 / 1.1
    count[3] /= 180000 / 1.1
    count[4] /= 120000 / 1.1
    count[5] /= 80000 / 1.1
    count[6] /= 70000 / 1.1
    count[7] /= 50000 / 1.1
    count[8] /= 40000 / 1.1
    count[9] /= 10000 / 1.1
    count[10] /= 10000 / 1.1
    // Avoid tripping false positives in below tests
    count[11] = 1

    // A perfect distribution would have all the counts be at 1.
    // Obviously, luck will make it so the values aren't perfect: the idea is
    // to verify a roughly correct distribution.
    expect(count.some((e:number) => e > 1.03)).toBeFalsy()
    expect(count.some((e:number) => e < 0.97)).toBeFalsy()
  })
})

describe('Generate elemental tuple', () => {
  test('We should not get an element not discovered yet', () => {
    const result : number[] = tuple.generateElementalTuple(Location.getLocationIdByName('Donjon K'), 3, 100000).map((e) => typeof e !== 'number' ? parseInt(e) : e)
    const count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let i = 0; i < result.length; i++) {
      count[result[i]] += 1
    }
    // There should be potions
    expect(count[0]).toBeGreaterThan(count[4])
    expect(count[1]).toBeGreaterThan(count[4])
    expect(count[2]).toBeGreaterThan(count[4])
    expect(count[3]).toBeGreaterThan(count[4])

    // There should be nothing higher than 3
    expect(count[4]).toBe(0)
    expect(count[5]).toBe(0)
    expect(count[6]).toBe(0)
    expect(count[7]).toBe(0)
    expect(count[8]).toBe(0)
    expect(count[9]).toBe(0)
    expect(count[10]).toBe(0)
    expect(count[11]).toBe(0)
  })
})

describe('Generate elemental tuple', () => {
  test('Our tuple depends on chain', () => {
    // Bibliothèque de glace
    const result : number[] = tuple.generateElementalTuple(Location.getLocationIdByName('Bibliothèque de glace'), 27, 100000).map((e) => typeof e !== 'number' ? parseInt(e) : e)
    const count = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for (let i = 0; i < result.length; i++) {
      count[result[i]]++
    }
    // There should be more potions than menthols
    expect(count[0]).toBeGreaterThan(count[4])
    expect(count[1]).toBeGreaterThan(count[4])
    expect(count[2]).toBeGreaterThan(count[4])
    expect(count[3]).toBeGreaterThan(count[4])

    // More menthols than eyes
    expect(count[4]).toBeGreaterThan(count[5])

    // More eyes than skulls
    expect(count[5]).toBeGreaterThan(count[6])

    // More skulls than flowers
    expect(count[6]).toBeGreaterThan(count[7])

    // We expect wind elements due to chain
    expect(count[7]).toBeGreaterThan(count[24])
    expect(count[24]).toBeGreaterThan(count[13])
    expect(count[24]).toBeGreaterThan(count[26])
    expect(count[26]).toBeGreaterThan(0)

    // There should be 0 elements from out of the chain
    expect(count[8]).toBe(0)
    expect(count[9]).toBe(0)
    expect(count[10]).toBe(0)
    expect(count[11]).toBe(0)
    expect(count[12]).toBe(0)
    expect(count[14]).toBe(0)
    expect(count[15]).toBe(0)
    expect(count[16]).toBe(0)
    expect(count[17]).toBe(0)
    expect(count[18]).toBe(0)
    expect(count[19]).toBe(0)
    expect(count[20]).toBe(0)
    expect(count[21]).toBe(0)
    expect(count[22]).toBe(0)
    expect(count[23]).toBe(0)

    // No max level element
    expect(count[27]).toBe(0)
  })
})
