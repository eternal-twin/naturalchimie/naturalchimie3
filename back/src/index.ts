import { express } from './lib/express'
const app = express()

// Reserving port n°
const port = 8333

const actRoutes = require('./act/act')

app.get('/', (req: any, res: any) => {
  res.send('Hello World!')
})

app.use('/act', actRoutes)

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
