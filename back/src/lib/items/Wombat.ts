import { BoardState } from '../BoardState'
import { MathUtils } from '../MathUtils'

export class Wombat {
  public static getNextMove (boardState: BoardState, currentX: number, currentY: number) {
    const moves: number[] = []
    // If we're touching the floor, we're going straight ahead
    if (currentY === 0) {
      return [0, 1]
    }
    // Check above
    if (currentY < 9) {
      if (boardState.getState()[currentY + 1][currentX].getId() !== -1) {
        moves.concat([0])
      }
    }
    // Check right
    if (currentX < 6) {
      if (boardState.getState()[currentY][currentX + 1].getId() !== -1) {
        moves.concat([1, 1])
      }
    }
    // Check below
    if (currentY > 0) {
      if (boardState.getState()[currentY - 1][currentX].getId() !== -1) {
        moves.concat([2, 2, 1])
      }
    }
    switch (moves[MathUtils.getRandomIntInclusive(0, moves.length-1)]) {
      case 0:
        return [1, 0]
      case 1:
        return [0, 1]
      case 2:
        return [-1, 0]
      default:
        return [0, 1]
    }
  }
}
