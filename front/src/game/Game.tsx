import React, { useEffect } from 'react';
import BootScene from './scenes/BootScene';
import MenuScene from './scenes/MenuScene';
import { useGameState } from 'src/hooks/GameStateContext';
import na3Game from './game/na3Game';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';

const Game: React.FC = () => {
  const gameState = useGameState();
  const currentLocation = useCurrentLocation();
  useEffect(() => {
    const game = new na3Game({
      parent: 'alchemyGame',
      height: 357.9,
      width: 204,
      scene: [BootScene, MenuScene],
    });
    game.setGameState(gameState);
    game.setCurrentLocation(currentLocation);
  }, []);

  return <div id="alchemyGame" />;
};

export default Game;
