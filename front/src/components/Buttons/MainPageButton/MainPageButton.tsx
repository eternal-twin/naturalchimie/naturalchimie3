import React, { useEffect, useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import Canvas from 'src/components/Canvas/Canvas';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';
import { ButtonStyle, drawShape, drawText } from './MainButtonDrawingService';

export interface MainPageButtonProps {
  text: string;
  onClick: () => void;
  disabled?: boolean;
}

const StyledButton = styled.div<{ isHovered: boolean }>`
  cursor: ${(props) => (props.isHovered ? 'pointer' : '')};
  height: 55px;
  width: 180px;
`;

const StyledCanvas = styled(Canvas)`
  position: absolute;
  z-index: 0;
`;

const StyledIcon = styled.img<{ height: number; width: number; x: number; y: number; disabled: boolean }>`
  height: ${(props) => props.height}px;
  width: ${(props) => props.width}px;
  margin-left: ${(props) => props.x}px;
  margin-top: ${(props) => props.y}px;
  position: absolute;
  z-index: 1;
  pointer-events: none;
  ${(props) => {
    if (props.disabled) return 'opacity:0.5;';
  }}
`;

const getButtonStyle = (
  text: string,
  disabled: boolean,
  formatMessage: (arg0: { id: string }) => string,
  isHovered: boolean
): ButtonStyle => {
  const isButtonPlay = text == formatMessage({ id: 'buttons.play' });
  const font = (isButtonPlay ? 'bold 18px' : '12px') + ' Helvetica';
  const baseStyle = { strokeWidth: 3, font };
  if (disabled) {
    return {
      ...baseStyle,
      fillColor: '#505050',
      strokeColor: '#413930',
      fontColor: '#a9a9a9',
    };
  }
  if (isButtonPlay) {
    return {
      ...baseStyle,
      fillColor: isHovered ? '#ffe256' : '#ef8200',
      strokeColor: '#ad5213',
      fontColor: '#ffad21',
    };
  }
  return {
    ...baseStyle,
    fillColor: isHovered ? '#83715c' : '#625545',
    strokeColor: '#4a3f33',
    fontColor: '#b39f87',
  };
};

const getIcon = (formatMessage: (arg0: { id: string }) => string, text: string, isSchoolCup: boolean): string => {
  switch (text) {
    case formatMessage({ id: 'buttons.play' }):
      return isSchoolCup
        ? assetsDictionnary['mainUi.rightMenu.buttonIcons.rank']
        : assetsDictionnary['mainUi.rightMenu.buttonIcons.play'];
    case formatMessage({ id: 'buttons.move' }):
      return assetsDictionnary['mainUi.rightMenu.buttonIcons.move'];
    default:
      return '';
  }
};

const MainPageButton: React.FC<MainPageButtonProps> = ({ disabled = false, ...props }) => {
  const [height, setHeight] = useState<number>(0);
  const [width, setWidth] = useState<number>(0);
  const { text } = props;
  const { formatMessage } = useIntl();
  const { isSchoolCup } = useCurrentLocation();
  const buttonRef = useRef<HTMLDivElement>(null);
  const [hover, setHover] = useState<boolean>(false);

  useEffect(() => {
    if (buttonRef.current) {
      setHeight(buttonRef.current.clientHeight);
      setWidth(buttonRef.current.clientWidth);
    }
  }, [buttonRef]);

  const drawButton = (ctx: CanvasRenderingContext2D, style: ButtonStyle): void => {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    drawShape(ctx, style);
    drawText(ctx, style, text);
  };

  const handleHover = (ctx: CanvasRenderingContext2D, e: React.MouseEvent): void => {
    const mouseX = e.clientX - ctx.canvas.getBoundingClientRect().left;
    const mouseY = e.clientY - ctx.canvas.getBoundingClientRect().top;
    if (ctx.isPointInPath(mouseX, mouseY)) {
      setHover(true);
    } else {
      setHover(false);
    }
  };

  return (
    <StyledButton ref={buttonRef} isHovered={hover} onClick={!disabled && hover ? props.onClick : undefined}>
      <StyledIcon
        src={getIcon(formatMessage, text, isSchoolCup)}
        height={height}
        width={height}
        x={Math.max(width / 5.5 - height / 2, 0)} //obtained by trial/error
        y={0}
        disabled={disabled}
      />
      <StyledCanvas
        height={height}
        width={width}
        draw={(ctx: CanvasRenderingContext2D) => drawButton(ctx, getButtonStyle(text, disabled, formatMessage, hover))}
        onMouseMove={
          !disabled ? (ctx: CanvasRenderingContext2D, e: React.MouseEvent) => handleHover(ctx, e) : undefined
        }
        onMouseLeave={() => setHover(false)}
      ></StyledCanvas>
    </StyledButton>
  );
};

export default MainPageButton;
