export interface ButtonStyle {
  strokeWidth: number;
  fillColor: string;
  strokeColor: string;
  font: string;
  fontColor: string;
}

export const drawShape = (ctx: CanvasRenderingContext2D, style: ButtonStyle): void => {
  const { strokeWidth } = style;
  ctx.fillStyle = style.fillColor;
  ctx.strokeStyle = style.strokeColor;
  ctx.lineWidth = strokeWidth;
  ctx.lineJoin = 'round';
  ctx.beginPath();

  //Proportions have been determined by trial/error to match current button design as much as possible.
  //All dimensions are related to the canvas height/width so it scales accordinlgy.
  ctx.moveTo(3 + strokeWidth, ctx.canvas.height / 2 - 1);
  ctx.lineTo(ctx.canvas.width / 5.5 - 1, 5 + strokeWidth);
  ctx.lineTo(ctx.canvas.width / 5.5 + 1, 5 + strokeWidth);
  ctx.lineTo(ctx.canvas.width / 3.5, (1.5 * ctx.canvas.height) / 5);
  ctx.lineTo(ctx.canvas.width - 3 - 2 - strokeWidth, (1.5 * ctx.canvas.height) / 5);
  ctx.lineTo(ctx.canvas.width - 3 - strokeWidth, (1.5 * ctx.canvas.height) / 5 + 2);
  ctx.lineTo(ctx.canvas.width - 3 - strokeWidth, (3.5 * ctx.canvas.height) / 5 - 2);
  ctx.lineTo(ctx.canvas.width - 3 - 2 - strokeWidth, (3.5 * ctx.canvas.height) / 5);
  ctx.lineTo(ctx.canvas.width / 3.5, (3.5 * ctx.canvas.height) / 5);
  ctx.lineTo(ctx.canvas.width / 5.5 + 1, ctx.canvas.height - 5 - strokeWidth);
  ctx.lineTo(ctx.canvas.width / 5.5 - 1, ctx.canvas.height - 5 - strokeWidth);
  ctx.lineTo(3 + strokeWidth, ctx.canvas.height / 2 + 1);
  ctx.closePath();
  ctx.fill();
  ctx.stroke();
};

export const drawText = (ctx: CanvasRenderingContext2D, style: ButtonStyle, text: string): void => {
  ctx.fillStyle = style.fontColor;
  ctx.font = style.font;
  ctx.fillText(text, ctx.canvas.width / 3.5 + 10, (3.5 * ctx.canvas.height) / 5 - 5);
};
