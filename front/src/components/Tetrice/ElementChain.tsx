import React from 'react';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import styled from 'styled-components';
import Canvas from '../Canvas/Canvas';
import assets from 'src/utils/assetsDictionnary';
import { TooltipBoxWrapper } from 'src/components/ui/Tooltips/TooltipWrapper';
import { useIntl } from 'react-intl';

const StyledElementsChain = styled.div`
  height: 600px;
  width: 100px;
  position: relative;
`;

const StyledImg = styled.img`
  height: 50px;
  width: 50px;
`;

const ElementChain: React.FC = () => {
  const { playerElements } = useCurrentLocation();

  const handleBackgroundDraw = (context: CanvasRenderingContext2D, backgroundUrl: string) => {
    const backgroundImage = new Image();
    backgroundImage.onload = () => context.drawImage(backgroundImage, 0, 0);
    backgroundImage.src = backgroundUrl;
  };

  const { formatMessage } = useIntl();
  return (
    <StyledElementsChain>
      <Canvas
        height={600}
        width={100}
        draw={(context: CanvasRenderingContext2D) =>
          handleBackgroundDraw(context, assets['game.elementChainBackground'])
        }
      />
      {playerElements.map((element, index) => {
        return (
          <TooltipBoxWrapper
            key={index}
            TooltippedElement={<StyledImg src={element.img} />}
            top={83 + 35 * index}
            left={22}
            content={element.description}
            name={element.name}
          />
        );
      })}
    </StyledElementsChain>
  );
};

export default ElementChain;
