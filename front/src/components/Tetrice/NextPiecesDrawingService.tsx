import { AlchemyElement, GameItem } from 'src/types';

const onLoadDropHandler = (
  ctx: CanvasRenderingContext2D,
  pieces: { img: HTMLImageElement; top: number; left: number; frameMultiplier?: number }[],
  frameCount: number
) => {
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  pieces.map((pieceImage) => {
    ctx.drawImage(
      pieceImage.img,
      pieceImage.left,
      ((frameCount * (pieceImage.frameMultiplier || 5)) % ctx.canvas.height) + pieceImage.top,
      35,
      35
    );
  });
};

const calculateInitPosition = (
  pieceImages: HTMLImageElement[],
  offset: { vOffset: number; hOffset: number },
  defaultPos: { top: number; left: number }
): { img: HTMLImageElement; top: number; left: number }[] => {
  return pieceImages.map((pieceImg, index) => ({
    img: pieceImg,
    top: defaultPos.top - (index < 2 ? 0 : 23) + (pieceImages.length <= 2 ? offset.vOffset : 0),
    left: defaultPos.left + (index % 2 == 0 ? 0 : 23) + (pieceImages.length == 1 ? offset.hOffset : 0),
  }));
};

export const handleDropDraw = (
  ctx: CanvasRenderingContext2D,
  frameCount: number,
  currentPieces: Array<AlchemyElement | GameItem>,
  offset: { vOffset: number; hOffset: number }
): void => {
  let count = 0;

  const currentPieceImages = currentPieces.map((piece) => {
    const pieceImage = new Image();
    pieceImage.src = piece.img;
    return pieceImage;
  });

  const positionedCurrentPieceImages = calculateInitPosition(currentPieceImages, offset, { top: 0, left: 7 });

  const positionedPieceImages = positionedCurrentPieceImages.map((p) => ({ ...p, frameMultiplier: 5 }));

  positionedPieceImages.map((pieceImage) => {
    pieceImage.img.onload = () => {
      count++;
      if (count === positionedPieceImages.length) {
        onLoadDropHandler(ctx, positionedPieceImages, frameCount);
      }
    };
  });
};
