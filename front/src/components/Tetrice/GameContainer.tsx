import React, { useEffect } from 'react';
import Game from 'src/game/Game';
import { useGameState } from 'src/hooks/GameStateContext';
import { DroppedPieces, AlchemyElement, GameItem } from 'src/types';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';
import ItemBelt from './ItemBelt';
import NextPieces from './NextPieces';
import Score from './Score';
import ApaScore from './ApaScore';

const StyledGameEngineContainer = styled.div`
  width: 205px;
  :hover {
    background-color: #fdffeb;
  }
`;

const StyledGameContainer = styled.div`
  background-repeat: no-repeat;
  background-position: top center;
  width: 477px;
  #ContainerHeader {
    background-image: url(${assetsDictionnary['game.container.backgroundHeader']});
    height: 100px;
  }
  #ContainerMain {
    background-image: url(${assetsDictionnary['game.container.backgroundMain']});
    padding: 8px 84px 60px 92px;
    height: 425px;
    box-sizing: border-box;
    display: flex;
    #ContainerSideColumn {
      background: linear-gradient(#3a3b3a, #786555);
      #Pyram {
        height: 35%;
      }
    }
  }
  #ContainerButtonSlice {
    background-image: url(${assetsDictionnary['game.container.backgroundRoll']});
    background-repeat: repeat-y;
  }
  #ContainerFooter {
    background-image: url(${assetsDictionnary['game.container.backgroundFooter']});
    height: 104px;
  }
`;
const StyledApaScore = styled(ApaScore)`
  float: inline-end;
  justify-content: flex-end;
  align-items: flex-end;
  margin-right: 3px;
`;

const GameContainer: React.FC = () => {
  const { pairDroppedEvent, nextPieces, setScore } = useGameState();

  //TODO: remove eventListener when Phaser takes over keyDown event and backend generates pieces
  const handleKeyDown = (event: KeyboardEvent) => {
    if (event.key === 'ArrowDown') {
      pairDroppedEvent(
        nextPieces[0].map((piece: AlchemyElement | GameItem) => ({
          element: piece,
          position: Math.floor(Math.random() * 6) + 1,
        })) as DroppedPieces
      );
      setScore(Math.floor(Math.random() * 100000));
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', handleKeyDown);
    return () => {
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  return (
    <StyledGameContainer>
      <div id="ContainerHeader" />
      <div id="ContainerMain">
        <StyledGameEngineContainer>{/* <Game /> */}</StyledGameEngineContainer>
        <div id="ContainerSideColumn">
          <Score />
          <StyledApaScore />
          <div id="Pyram"></div>
          <NextPieces />
          <ItemBelt />
        </div>
      </div>
      <div id="ContainerButtonSlice" />
      <div id="ContainerFooter" />
    </StyledGameContainer>
  );
};

export default GameContainer;
