import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useGameState } from 'src/hooks/GameStateContext';
import CustomRadioButton from '../ui/CustomRadioButton';

const StyledApaScore = styled.div`
  display: flex;
  height: 14px;
  width: 70px;
  background: #695e5c;
  box-shadow: inset 0px 3px 1px #5d595a, 0px 3px 2px #3c3837;
  border-bottom-left-radius: 8px;
  font-size: 11px;
  text-shadow: 0px 0px 0px #b19580, 1px 1px 2px rgba(76, 66, 54, 1);
  color: transparent;
  font-family: 'LondonTwo';
  padding-right: 3px;
  justify-content: flex-end;
  align-items: flex-end;
  }
`;

const StyledRadioButton = styled(CustomRadioButton)`
  position: relative;
  right: 22%;
  bottom: 25%;
`;

interface ApaScoreProps {
  className?: string;
}

const ApaScore: React.FC<ApaScoreProps> = (props) => {
  const [isCompleted, setIsCompleted] = useState(false);
  const { apaScore, score } = useGameState();

  useEffect(() => {
    setIsCompleted(score >= apaScore);
  }, [score]);

  return (
    <StyledApaScore className={props.className}>
      {isCompleted ? (
        <StyledRadioButton color={'green'} size={'8px'} />
      ) : (
        <StyledRadioButton color={'gray'} size={'6px'} />
      )}
      <span>{apaScore}</span>
    </StyledApaScore>
  );
};

export default ApaScore;
