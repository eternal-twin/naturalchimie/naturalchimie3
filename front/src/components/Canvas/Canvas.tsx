import React, { useRef, useEffect } from 'react';

export interface CanvasProps {
  className?: string;
  width: number;
  height: number;
  draw: (context: CanvasRenderingContext2D) => void;
  onClick?: () => void;
  onMouseMove?: (context: CanvasRenderingContext2D, e: React.MouseEvent) => void;
  onMouseLeave?: (context: CanvasRenderingContext2D) => void;
}

const Canvas: React.FC<CanvasProps> = (props) => {
  const { draw, onMouseMove, onMouseLeave, ...rest } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    if (canvasRef.current) {
      const canvas = canvasRef.current;
      const context = canvas.getContext('2d');
      if (context) {
        draw(context);
      }
    }
  }, [draw]);

  const handleHover = (e: React.MouseEvent) => {
    if (onMouseMove) {
      e.preventDefault();
      e.stopPropagation();
      if (canvasRef.current) {
        const canvas = canvasRef.current;
        const context = canvas.getContext('2d');
        if (context) {
          onMouseMove(context, e);
        }
      }
    }
  };

  const handleMouseLeave = (e: React.MouseEvent) => {
    if (onMouseLeave) {
      e.preventDefault();
      e.stopPropagation();
      if (canvasRef.current) {
        const canvas = canvasRef.current;
        const context = canvas.getContext('2d');
        if (context) {
          onMouseLeave(context);
        }
      }
    }
  };
  return (
    <canvas ref={canvasRef} onMouseMove={(e) => handleHover(e)} onMouseLeave={(e) => handleMouseLeave(e)} {...rest} />
  );
};

export default Canvas;
