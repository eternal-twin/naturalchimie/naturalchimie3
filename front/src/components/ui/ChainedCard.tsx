import React from 'react';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';

const StyledCard = styled.div`
  border-image-source: url(${assetsDictionnary['mainUi.chainedBorder']});
  border-image-outset: 5px;
  border-image-repeat: round;
  border-image-width: 25px 20px 39px 20px;
  border-image-slice: 34.8% 35% 52.2% fill;
  padding: 15px 15px 25px;
  color: #e6cfb8;
  font-size: 12pt;
  max-width: 60%;
`;

const ChainedCard: React.FC<{ className?: string; content: string }> = (props) => {
  return <StyledCard className={props.className}>{props.content}</StyledCard>;
};

export default ChainedCard;
