import React from 'react';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';
import Menu from './Menu';

const StyledHeader = styled.div`
  position: relative;
  height: 104px;
`;

const StyledLogo = styled.div`
  position: absolute;
  :before {
    content: url(${assetsDictionnary['mainUi.header.headerLogo']});
  }
  z-index: 0;
`;

const StyledMenu = styled(Menu)`
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 1;
`;

const Header: React.FC = () => {
  return (
    <StyledHeader>
      <StyledLogo />
      <StyledMenu />
    </StyledHeader>
  );
};

export default Header;
