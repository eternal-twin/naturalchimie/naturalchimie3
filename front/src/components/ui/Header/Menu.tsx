import React, { useState } from 'react';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';

//Doesn't work if the justify-content is added directly to the AnimationWrapper
const StyledFoldableMenu = styled.div`
  display: flex;
  justify-content: right;
`;

const AnimationWrapper = styled.div<{ isVisible: boolean }>`
  display: flex;
  transition: max-width 0.8s, padding-right 0.9s;
  max-width: ${(props) => (props.isVisible ? '100%' : '30px')};
  padding-right: ${(props) => (props.isVisible ? '0' : '50px')};
  #StyledMenu {
    transform: translateX(${(props) => (props.isVisible ? '-28' : '15')}px);
    transition: transform ${(props) => (props.isVisible ? '0.2' : '2.5')}s;
  }
`;

const StyledMenu = styled.div`
  overflow: hidden;
  :before {
    content: url(${assetsDictionnary['mainUi.header.headerMenu']});
  }
`;

const StyledArrow = styled.div`
  padding-right: 30px;
  align-self: center;
  flex-basis: auto;
  padding-left: 10px;
  padding-top: 5px;
  border-top-left-radius: 15px;
  border-bottom-left-radius: 15px;
  box-shadow: -2px 2px 4px #4d4236, -4px 4px 2px #4d4236;
  background: linear-gradient(to top, #a38c73 0%, #a38c73 40%, #907b65 41%, #907b65 65%, #b3997f 66%);
  img {
    height: 50px;
    filter: drop-shadow(1px 1px 2px #4d4236);
    :hover {
      filter: brightness(1.2) drop-shadow(1px 1px 2px #4d4236);
    }
  }
`;

const StyledMenuIcon = styled.img<{ index: number }>`
  position: absolute;
  left: ${(props) => 5 + (props.index - 1) * 78}px;
  :hover {
    margin-top: 2px;
    filter: brightness(1.2);
  }
`;

const Menu: React.FC<{ className?: string }> = (props) => {
  const [isMenuVisible, setIsMenuVisible] = useState<boolean>(true);

  const handleArrowClick = () => {
    setIsMenuVisible(!isMenuVisible);
  };
  return (
    <StyledFoldableMenu className={props.className}>
      <AnimationWrapper isVisible={isMenuVisible}>
        <StyledArrow onClick={() => handleArrowClick()}>
          <img
            src={
              isMenuVisible
                ? assetsDictionnary['mainUi.header.arrowRight']
                : assetsDictionnary['mainUi.header.arrowLeft']
            }
          ></img>
        </StyledArrow>
        <StyledMenu id={'StyledMenu'}>
          <StyledMenuIcon src={assetsDictionnary['mainUi.header.menuIcons.guildian']} index={1} />
          <StyledMenuIcon src={assetsDictionnary['mainUi.header.menuIcons.map']} index={2} />
          <StyledMenuIcon src={assetsDictionnary['mainUi.header.menuIcons.cex']} index={3} />
          <StyledMenuIcon src={assetsDictionnary['mainUi.header.menuIcons.bank']} index={4} />
          <StyledMenuIcon src={assetsDictionnary['mainUi.header.menuIcons.help']} index={5} />
        </StyledMenu>
      </AnimationWrapper>
    </StyledFoldableMenu>
  );
};

export default Menu;
