import React from 'react';
import MainPageButton from 'src/components/Buttons/MainPageButton/MainPageButton';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import ElementCard from './ElementCard';
import { useHistory } from 'react-router-dom';
import LocationCard from './LocationCard';

const StyledRightMenu = styled.div`
  width: 200px;
  padding: 5px 10px;
  border-radius: 10px;
  background-color: #a38c73;
  box-shadow: inset 0px 0px 3px #907b65, 3px 0px 3px #4d4236;
`;

const StyledLocationCard = styled(LocationCard)`
  margin: 10px 0;
`;

interface RigthMenuProps {
  isMoving: boolean;
  isTalking: boolean;
  setIsMoving: React.Dispatch<React.SetStateAction<boolean>>;
}

const RightMenu: React.FC<RigthMenuProps> = ({ isTalking, isMoving, setIsMoving }) => {
  const { formatMessage } = useIntl();
  const history = useHistory();

  const onPlayClickHandler = () => {
    history.push('/act/play');
  };

  const onMoveClickHandler = () => {
    setIsMoving(!isMoving);
  };

  return (
    <StyledRightMenu>
      <StyledLocationCard />
      <ElementCard />
      <MainPageButton
        text={formatMessage({ id: 'buttons.play' })}
        onClick={onPlayClickHandler}
        disabled={isTalking}
      ></MainPageButton>
      <MainPageButton
        text={formatMessage({ id: 'buttons.move' })}
        onClick={onMoveClickHandler}
        disabled={isTalking}
      ></MainPageButton>
    </StyledRightMenu>
  );
};

export default RightMenu;
