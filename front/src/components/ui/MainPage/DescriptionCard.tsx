import React from 'react';
import styled from 'styled-components';
import ChainedCard from 'src/components/ui/ChainedCard';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import { Faction } from 'src/types';

const StyledCard = styled(ChainedCard)<{ faction: Faction }>`
  ::first-letter {
    font-size: 200%;
    font-weight: bold;
    float: left;
    position: relative;
    margin-right: 5px;
  }
  line-height: 100%;
  padding-bottom: 30px;
  ${(props) => {
    switch (props.faction) {
      case Faction.GUILD:
        return ``;
      case Faction.SHANGKAH:
        return `
        ::first-letter {
            color: #cf381a;
        }
        color: #fee6b4`;
      case Faction.AUDEPINT:
        return `
        ::first-letter {
            color: #ffb521;
        }
        color: #c8e0ea`;
      case Faction.JEEZARA:
        return `
        ::first-letter {
            color: #801ac4;
        }
        color: #d7acf4`;
      case Faction.GEMINI:
        return `
        ::first-letter {
            color: #f76d18;
        }
        color: #e9ee87`;
    }
  }}
`;

const DescriptionCard: React.FC<{ className?: string }> = (props) => {
  //const { faction, description } = useCurrentLocation(); //Need NA-52 merged to develop to work.
  const { description } = useCurrentLocation();
  const faction = Faction.GUILD;
  return <StyledCard faction={faction} content={description} className={props.className} />;
};

export default DescriptionCard;
