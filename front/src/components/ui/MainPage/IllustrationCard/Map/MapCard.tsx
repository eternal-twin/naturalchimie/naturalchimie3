import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';

const StyledMapCard = styled.img``;

//TODO see epic NA-57
const MapCard: React.FC<{ className?: string }> = (props) => {
  return <StyledMapCard src={assetsDictionnary['map']} className={props.className} />;
};

export default MapCard;
