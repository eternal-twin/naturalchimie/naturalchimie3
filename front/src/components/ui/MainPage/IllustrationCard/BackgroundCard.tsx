import React from 'react';
import styled from 'styled-components';

const StyledBackgroundCard = styled.img``;

const BackgroundCard: React.FC<{ img: string; className?: string }> = (props) => {
  return <StyledBackgroundCard src={props.img} className={props.className} />;
};

export default BackgroundCard;
