import React, { useState } from 'react';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import BackgroundCard from './BackgroundCard';
import MapCard from './Map/MapCard';

const StyledCard = styled.div`
  position: relative;
  min-height: 300px;
  min-width: 500px;
  border-radius: 2px;
  overflow: hidden;
`;

const StyledForeground = styled.img`
  position: absolute;
  z-index: 1;
  width: 100%;
`;

const StyledBackground = styled(BackgroundCard)`
  position: absolute;
  overflow: hidden;
  z-index: 0;
  width: 100%;
  transform: translateY(-10%);
`;

const StyledMap = styled(MapCard)`
  position: absolute;
  overflow: hidden;
  z-index: 0;
  width: 100%;
`;

interface IllustrationCardProps {
  isMoving: boolean;
  className?: string;
}

const IllustrationCard: React.FC<IllustrationCardProps> = (props) => {
  const { isMoving } = props;
  const { background } = useCurrentLocation();
  return (
    <StyledCard className={props.className}>
      <StyledForeground src={assetsDictionnary['mainUi.illustrationForeground']} />
      {isMoving ? <StyledMap /> : <StyledBackground img={background} />}
    </StyledCard>
  );
};

export default IllustrationCard;
