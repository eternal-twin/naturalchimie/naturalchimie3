import React from 'react';
import { useIntl } from 'react-intl';
import { useCurrentLocation } from 'src/hooks/CurrentLocationContext';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import styled from 'styled-components';
import CustomRadioButton from '../CustomRadioButton';
import { TooltipBoxWrapper } from '../Tooltips/TooltipWrapper';

const StyledLocationCard = styled.div`
  color: #e6cfb8;
  background: linear-gradient(135deg, #bea487 0%, #ab9379 10%, #8f7b65 50%, #857767 80%, #7b7064 100%);
  border: 3px solid #3a3127;
  border-radius: 10px;
  box-shadow: 2px 1px 1px #605041;
  z-index: 0;
`;

const StyledInnerDiv = styled.div`
  display: flex;
  align-items: center;
  margin: 3px;
  border: 4px solid #5c4f41;
  border-radius: 6px;
  padding: 2px;
  background: linear-gradient(135deg, #bea487 0%, #ab9379 10%, #8f7b65 50%, #857767 80%, #7b7064 100%);
`;

const StyledLogo = styled.img`
  height: 25px;
  padding-right: 5px;
`;

const StyledText = styled.div`
  font-weight: bold;
  font-size: 14px;
  line-height: 14px;
`;

const StyledMagicItem = styled.div`
  color: #b39a7f;
  font-size: x-small;
  background: linear-gradient(176deg, #5c4d3e 0%, #4d4034 40%, #3d3329 60%, #3a3127 100%);
  margin-left: 24%;
  margin-right: 26%;
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  z-index: 1;
`;

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`;

const LocationCard: React.FC<{ className?: string }> = ({ className }) => {
  const { faction, name, areMagicItemsAllowed } = useCurrentLocation();
  const { formatMessage } = useIntl();

  return (
    <div className={className}>
      <TooltipBoxWrapper
        TooltippedElement={
          <StyledMagicItem>
            <StyledDiv>
              <span>{formatMessage({ id: 'mainPage.magicObjects' })}</span>
              <CustomRadioButton color={areMagicItemsAllowed ? 'green' : 'brown'} size={'7px'} />
            </StyledDiv>
          </StyledMagicItem>
        }
        content={formatMessage({
          id: areMagicItemsAllowed ? 'mainPage.magicObjectsAllowed' : 'mainPage.magicObjectsNotAllowed',
        })}
      />
      <StyledLocationCard>
        <StyledInnerDiv>
          <StyledLogo src={assetsDictionnary['mainUi.schools.icon.' + faction]} />
          <StyledText>{name}</StyledText>
        </StyledInnerDiv>
      </StyledLocationCard>
    </div>
  );
};

export default LocationCard;
