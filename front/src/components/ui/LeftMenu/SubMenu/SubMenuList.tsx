export interface menuItemProps {
  name: string;
  path: string;
  leftPos: number;
  topPos: number;
}

export const SubMenuItemList = [
  { name: 'classIcon', path: '', leftPos: 100, topPos: 75 },
  { name: 'crystalBallIcon', path: '', leftPos: 40, topPos: 75 },
  { name: 'grimoireIcon', path: '/book', leftPos: 135, topPos: 20 },
  { name: 'inventoryIcon', path: '/act/bag', leftPos: 10, topPos: 15 },
  { name: 'personalPageIcon', path: '', leftPos: 0, topPos: 0 },
];

export default SubMenuItemList;
