import React from 'react';
import styled from 'styled-components';

const StyledHalfDisc = styled.div<{ width: number }>`
  width: ${(props) => props.width + 'px'};
  height: ${(props) => props.width / 2 + 'px'};
  border-bottom-left-radius: ${(props) => props.width / 2 + 'px'};
  border-bottom-right-radius: ${(props) => props.width / 2 + 'px'};
  background: radial-gradient(
    ellipse at top,
    #363c40,
    #4a5157 25%,
    transparent 25%,
    transparent 46%,
    #4a5157 46%,
    #4a5157 100%
  );
  border-top: 0;
  box-shadow: 0 2px 2px #2e2620, 0 3px 2px #2e2620, inset 0 -1px 3px #6d7478;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
`;

const HalfDisc: React.FC<{ width?: number; className?: string }> = ({ width, className }) => {
  return <StyledHalfDisc width={width ?? 25} className={className} />;
};

export default HalfDisc;
