import React from 'react';
import { AlchemyElement, GameItem } from 'src/types';
import styled from 'styled-components';
import { TooltipBoxWrapper } from '../Tooltips/TooltipWrapper';

export interface ItemComponentType {
  item: AlchemyElement | GameItem;
  className?: string;
}

const StyledImg = styled.img`
  height: 100%;
  width: 100%;
`;

const ItemComponent: React.FC<ItemComponentType> = ({ item, className }) => {
  return (
    <div className={className}>
      <TooltipBoxWrapper
        TooltippedElement={<StyledImg src={item.img} />}
        name={item.name}
        content={item.description}
        left={0}
        top={0}
      />
    </div>
  );
};

export default ItemComponent;
