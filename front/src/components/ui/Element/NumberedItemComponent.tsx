import React from 'react';
import { AlchemyElement, GameItem } from 'src/types';
import styled from 'styled-components';
import { TooltipBoxWrapper } from '../Tooltips/TooltipWrapper';

export interface NumberedItemComponentType extends React.HTMLAttributes<HTMLElement> {
  amountInStock: number;
  totalRequired?: number;
  item: AlchemyElement | GameItem;
  className?: string;
  attrs?: styledProps;
}

export interface styledProps {
  size?: string;
  minSize?: string;
  padding?: { top: string; bottom: string; left: string; right: string };
}

const StyledAmount = styled.div`
  display: block;
  position: absolute;
  bottom: 3%;
  left: 9%;
  text-align: center;
  font-family: 'LondonTwo';
  font-size: 14px;
  text-shadow: 0px 0px 0px #d6f8ff, 2px -1px 1px #1d3541, -1px -1px 1px #1d3541;
  color: transparent;
`;

const StyledUsable = styled.div`
  display: block;
  position: absolute;
  top: 10%;
  left: 9%;
  text-align: center;
  font-family: 'Tahoma';
  font-size: 13px;
  text-shadow: 0px 0px 0px #ffdd11, 0px 0px 0px #ffed62, 1px -1px 1px #ab2200, -1px -1px 1px #861300;
  color: transparent;
`;

const ComponentWrapper = styled.div.attrs((props: styledProps) => ({
  size: props.size || '50px',
  minSize: props.minSize || props.size || '50px',
}))`
  display: block;
  height: ${(props) => props.size};
  width: ${(props) => props.size};
  min-height: ${(props) => props.minSize};
  min-width: ${(props) => props.minSize};
  position: relative;
`;

const StyledImg = styled.img`
  height: 100%;
  width: 100%;
`;

const NumberedItemComponent: React.FC<NumberedItemComponentType> = (props) => {
  const { item, className, amountInStock, totalRequired, attrs } = props;
  return (
    <ComponentWrapper className={className} {...attrs}>
      <TooltipBoxWrapper
        TooltippedElement={
          <>
            <StyledUsable> {item.usable ? '!' : ''}</StyledUsable>
            <StyledImg src={item.img} />
            <StyledAmount>
              {totalRequired != undefined ? amountInStock + ' / ' + totalRequired : amountInStock}
            </StyledAmount>
          </>
        }
        name={item.name}
        content={item.description}
        left={0}
        top={0}
      />
    </ComponentWrapper>
  );
};

export default NumberedItemComponent;
