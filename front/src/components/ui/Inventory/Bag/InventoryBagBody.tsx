import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useUserContext } from 'src/hooks/userContext';
import NumberedItemComponent from '../../Element/NumberedItemComponent';

const BagBody = styled.div`
  width: 100%;
  min-width: 500px;
  background: url(${assetsDictionnary['inventory.bag.bagBody']}) top center;
  background-size: 90%;
  background-repeat: repeat-y;
`;

const InventoryWrapper = styled.div`
  margin: 0 11.5%;
  display: flex;
  flex-wrap: wrap;
`;

const ItemWrapper = styled.div``;

const InventoryBagBody: React.FC<{ className?: string }> = ({ className }) => {
  const { inventory } = useUserContext();

  return (
    <BagBody className={className}>
      <InventoryWrapper>
        {inventory.map((invItem, index) => {
          return (
            <ItemWrapper key={index}>
              <NumberedItemComponent
                item={invItem.item}
                amountInStock={invItem.count}
                attrs={{ size: '3.9vw', minSize: '40px' }}
              />
            </ItemWrapper>
          );
        })}
      </InventoryWrapper>
    </BagBody>
  );
};

export default InventoryBagBody;
