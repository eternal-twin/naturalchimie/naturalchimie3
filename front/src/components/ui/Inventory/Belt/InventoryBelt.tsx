import React from 'react';
import styled from 'styled-components';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useUserContext } from 'src/hooks/userContext';
import InventoryBeltSlot from './InventoryBeltSlot';

const StyledSlotWrapper = styled.div<{ index: number }>`
  position: absolute;
  width: 7.8%;
  height: 7.8%;
  min-height: 68px;
  margin-left: ${(props) => 29.1 + props.index * (7.8 + 5.4)}%;
  margin-top: 1.7%;
`;

const StyledBelt = styled.div`
  position: relative;
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 90%;
  min-width: 200px;
  height: 10%;
  min-height: 75px;
  background: url(${assetsDictionnary['inventory.belt']}) no-repeat top left;
  background-size: 100%;
`;

const InventoryBelt: React.FC<{ className?: string }> = ({ className }) => {
  const { itemBelt } = useUserContext();

  return (
    <StyledBelt className={className}>
      {itemBelt.map((slot, index) => {
        return (
          <StyledSlotWrapper key={index} index={index}>
            <InventoryBeltSlot slot={slot} />
          </StyledSlotWrapper>
        );
      })}
    </StyledBelt>
  );
};

export default InventoryBelt;
