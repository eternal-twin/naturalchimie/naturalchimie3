import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { TooltipBoxWrapper } from '../../Tooltips/TooltipWrapper';
import { BeltSlot, GameItem, Inventory, isGameItem } from 'src/types';
import ItemSelector from './ItemSelector';
import { useUserContext } from 'src/hooks/userContext';

const filterInventory = (
  inventory: Inventory,
  currentItem: GameItem | null
): Array<{ count: number; item: GameItem }> => {
  const result = (inventory.filter((item) => {
    return (
      isGameItem(item.item) &&
      item.item.equippable &&
      item.availableForBelt !== undefined &&
      item.availableForBelt !== 0 &&
      (currentItem === null || item.item.id !== currentItem.id)
    );
  }) as Array<{ count: number; availableForBelt: number; item: GameItem }>).map((inventoryItem) => ({
    item: inventoryItem.item,
    count: inventoryItem.availableForBelt,
  }));
  console.log({ result });
  return result;
};

const StyledInventorySlot = styled.div<{ isSlotClosed: boolean }>`
  position: absolute;
  ${(props) => (props.isSlotClosed ? '' : `background: url(${assetsDictionnary['inventory.beltSlot']}) no-repeat;`)}
  background-size: 100%;
  width: 100%;
  height: 100%;
`;

const StyledImg = styled.img`
  height: 100%;
  width: 100%;
`;

const StyledItemSelector = styled(ItemSelector)<{ slotNumber: number }>`
  position: absolute;
  top: 100%;
  left: ${(props) => -300 * (1 + props.slotNumber / 5)}%;
`;

const InventoryBeltSlot: React.FC<{ slot: BeltSlot }> = ({ slot }) => {
  const { formatMessage } = useIntl();
  const { itemBelt, inventory } = useUserContext();
  const itemSelectorRef = useRef<HTMLImageElement>(null);
  const [isSelectorVisible, setIsSelectorVisible] = useState(false);
  const [slotId] = useState('belt_slot_' + slot.pocketIndex);
  const { updateBeltContent } = useUserContext();
  const [itemListToDisplay, setItemListToDisplay] = useState<Array<{ count: number; item: GameItem }>>([]);

  const handleClickOutside = (event: MouseEvent) => {
    const target = event.target as Node;
    if (
      itemSelectorRef.current &&
      !itemSelectorRef.current.contains(target) &&
      (target as HTMLElement).getAttribute('id') != slotId
    ) {
      setIsSelectorVisible(false);
    }
  };

  const onClickHandler = (event: React.MouseEvent<HTMLImageElement, MouseEvent>) => {
    event.preventDefault();
    setIsSelectorVisible(!isSelectorVisible);
  };

  const itemSelectionHandler = (item: GameItem, event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    event.preventDefault();
    const newSlot = { ...slot, item };
    updateBeltContent(newSlot);
  };

  useEffect(() => {
    document.addEventListener('click', handleClickOutside, true);
    return () => {
      document.removeEventListener('click', handleClickOutside, true);
    };
  }, []);

  useEffect(() => {
    setItemListToDisplay(filterInventory(inventory, slot.item));
    setIsSelectorVisible(false);
  }, [slot]);

  useEffect(() => {
    setItemListToDisplay(filterInventory(inventory, slot.item));
  }, [itemBelt]);

  return (
    <StyledInventorySlot isSlotClosed={slot.isLocked}>
      <TooltipBoxWrapper
        TooltippedElement={
          !slot.isLocked ? (
            <StyledImg id={slotId} src={slot.item ? slot.item.img : ''} onClick={(event) => onClickHandler(event)} />
          ) : (
            <div style={{ height: '50px', width: '50px' }}></div>
          )
        }
        name={slot.item?.name ?? ''}
        content={slot.item?.description ?? formatMessage({ id: 'inventoryPage.beltSlotLocked' })}
        left={0}
        top={0}
      />
      <StyledItemSelector
        innerRef={itemSelectorRef}
        isVisible={isSelectorVisible}
        slotNumber={slot.pocketIndex}
        itemListToDisplay={itemListToDisplay}
        updateSelectedItem={itemSelectionHandler}
      />
    </StyledInventorySlot>
  );
};

export default InventoryBeltSlot;
