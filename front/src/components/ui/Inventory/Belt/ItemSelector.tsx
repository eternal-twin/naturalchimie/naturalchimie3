import React, { RefObject, useRef, useState } from 'react';
import styled from 'styled-components';
import { GameItem } from 'src/types';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import NumberedItemComponent from '../../Element/NumberedItemComponent';

const StyledSelector = styled.div<{ isVisible: boolean }>`
  ${(props) =>
    props.isVisible
      ? `
    display: flex; 
    justify-content: center;`
      : `
    display: none;`}
  border-image-source: url(${assetsDictionnary['inventory.itemSelector']});
  border-image-outset: 5px;
  border-image-repeat: round;
  border-image-width: 18% 12%;
  border-image-slice: 22% 7% fill;
  padding: 15px 15px 25px;
  height: 100px;
  width: 500px;
`;

const InventoryScrollWrapper = styled.div`
  position: relative;
  display: grid;
  grid-template-rows: auto auto;
  grid-auto-flow: column;
  grid-auto-columns: min-content;
  overflow: hidden;
  max-height: 110%;
  width: 78%;
`;

const StyledArrowLeft = styled.img`
  position: absolute;
  top: 22%;
  left: 28px;
  height: 55%;
  width: 5%;
  filter: drop-shadow(1px 1px 2px #4d4236);
  :hover {
    filter: brightness(1.2) drop-shadow(1px 1px 2px #4d4236);
  }
  z-index: 2;
`;

const StyledArrowRight = styled.img`
  position: absolute;
  top: 22%;
  right: 28px;
  height: 55%;
  width: 5%;
  filter: drop-shadow(1px 1px 2px #4d4236);
  :hover {
    filter: brightness(1.2) drop-shadow(1px 1px 2px #4d4236);
  }
  z-index: 2;
`;

interface ItemSelectorProps {
  innerRef?: RefObject<HTMLImageElement>;
  className?: string;
  isVisible: boolean;
  itemListToDisplay: Array<{ count: number; item: GameItem }>;
  updateSelectedItem: (item: GameItem, event: React.MouseEvent<HTMLElement, MouseEvent>) => void;
}

const ItemSelector: React.FC<ItemSelectorProps> = (props) => {
  const { updateSelectedItem, className, isVisible, innerRef, itemListToDisplay } = props;
  const scrl = useRef<HTMLDivElement>(null);
  const [scrollX, setscrollX] = useState(0);
  const [scrollEnd, setscrollEnd] = useState(false);

  const onArrowClickHandler = (shift: number) => {
    if (scrl.current) {
      scrl.current.scrollLeft += shift;
      setscrollX(scrollX + shift);
      if (Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <= scrl.current.offsetWidth) {
        setscrollEnd(true);
      } else {
        setscrollEnd(false);
      }
    }
  };

  const scrollCheck = () => {
    if (scrl.current) {
      setscrollX(scrl.current.scrollLeft);
      if (Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <= scrl.current.offsetWidth) {
        setscrollEnd(true);
      } else {
        setscrollEnd(false);
      }
    }
  };

  return (
    <StyledSelector ref={innerRef} className={className} isVisible={isVisible}>
      <StyledArrowLeft
        src={assetsDictionnary['mainUi.header.arrowLeft']}
        onClick={() => scrollX > 0 && onArrowClickHandler(-50)}
      />
      <InventoryScrollWrapper ref={scrl} onScroll={scrollCheck}>
        {itemListToDisplay.map((item, index) => {
          return (
            <div key={index} onClick={(event) => updateSelectedItem(item.item, event)}>
              <NumberedItemComponent item={item.item} amountInStock={item.count} />
            </div>
          );
        })}
      </InventoryScrollWrapper>
      <StyledArrowRight
        src={assetsDictionnary['mainUi.header.arrowRight']}
        onClick={() => !scrollEnd && onArrowClickHandler(50)}
      />
    </StyledSelector>
  );
};

export default ItemSelector;
