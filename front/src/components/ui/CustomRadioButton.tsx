import React from 'react';
import styled from 'styled-components';

const StyledRadioButton = styled.span<{ color: string; size?: string }>`
  height: ${(props) => props.size || '6px'};
  width: ${(props) => props.size || '6px'};
  ${(props) => {
    switch (props.color) {
      case 'green':
        return `box-shadow: inset 1px 1px 2px #739536, inset -1px -1px 2px #739536, 0px 0px 5px #546b25;
            background-color: #bde960;
            border-radius: 50%;
            display: inline-block;`;
      case 'gray':
        return `box-shadow: inset 0px 0px 2px #434a54, inset -1px -1px 2px #434a54, 0px 0px 1px #3c3b40;
            background-color: #585f69;
            border: 1px solid #3c3b40;
            border-radius: 50%;
            display: inline-block;`;
      case 'brown':
        return `box-shadow: inset 0px 0px 2px #5a4c3d, inset -1px -1px 2px #776757;
        background-color: #b6a797;
        border: 1px solid #000000;
        border-radius: 50%;
        display: inline-block;`;
    }
  }}
`;

interface RadioButtonProps {
  color: string;
  className?: string;
  size?: string;
}

const CustomRadioButton: React.FC<RadioButtonProps> = (props) => {
  return <StyledRadioButton {...props} />;
};

export default CustomRadioButton;
