import React from 'react';
import styled from 'styled-components';
import { useIntl } from 'react-intl';

import MainLayout from './MainLayout';
import InventoryBeltCard from '../ui/Inventory/Belt/InventoryBeltCard';
import InventoryBag from '../ui/Inventory/Bag/InventoryBag';

const StyledTitle = styled.h2`
  ::first-letter {
    letter-spacing: 0px;
    color: #b39a7f;
    font-size: 120%;
    background-color: #675949;
    padding: 0 7px 0 7px;
  }
  font-size: 30px;
  padding-left: 10px;
  padding-right: 10px;
  letter-spacing: 1px;
  color: #e6cfb8;
`;

const Content = styled.div`
  min-width: 100px;
  width: 80%;
`;

const InventoryLayout: React.FC = () => {
  const { formatMessage } = useIntl();
  return (
    <MainLayout
      C={
        <Content>
          <div>
            <StyledTitle>{formatMessage({ id: 'inventoryPage.bagTitle' })}</StyledTitle>
            <InventoryBag />
          </div>
          <div>
            <StyledTitle>{formatMessage({ id: 'inventoryPage.beltTitle' })}</StyledTitle>
            <InventoryBeltCard />
          </div>
        </Content>
      }
    />
  );
};

export default InventoryLayout;
