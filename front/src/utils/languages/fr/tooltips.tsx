const tooltips = {
  classIcon: "Voir les différentes classes de l'école.",
  crystalBallIcon: 'Votre boule à papote, pour accéder aux forums.',
  grimoireIcon: 'Votre grimoire.',
  inventoryIcon: 'Votre inventaire.',
  personalPageIcon: 'Votre page personnelle.',
};

export default tooltips;
