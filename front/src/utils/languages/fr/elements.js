export default {
  name: {
    greenPotion: 'Potion Verte',
    yellowPotion: 'Potion Jaune',
    orangePotion: 'Potion Orange',
    purplePotion: 'Potion Violette',
    mentholLeaf: 'Feuille Menthol',
    crazeye: 'Foleil',
    skullie: 'Cranos',
    flawer: 'Flaum',
    oxide: 'Oxyde',
    coppa: 'Couivre',
    quicksilvyr: 'Mercurule',
    goldNugget: "Pépite d'Or",
  },
  description: {
    greenPotion: 'Un élément alchimique des plus courants. Valeur: 1 point.',
    yellowPotion: 'Un élément alchimique des plus courants. Valeur: 3 points.',
    orangePotion: 'Un élément alchimique des plus courants. Valeur: 9 points.',
    purplePotion: 'Un élément alchimique des plus courants. Valeur: 27 points.',
    mentholLeaf:
      "Premier élément alchimique complexe, la Feuille Menthol n'est pas comestible en salade. Valeur: 81 points.",
    crazeye: `Nos ancêtres pensaient que croiser le regard d'un Foleil 
                  portait malheur à leur alchimie. On a prouvé depuis que c'était n'importe quoi. Valeur: 243 points.`,
    skullie: `Element alchimique le plus macabre, le Cranos peut faire tourner de l'oeil les alchimistes
                   les plus sensibles quand il apparait. Valeur: 729 points.`,
    flawer: 'Dernier élément standard, il est indispensable pour les recettes 100% bio. Valeur: 2187 points.',
    oxide: 'Le premier élément métallique. Valeur: 6 561 points.',
    coppa: 'Le deuxième élément métallique. Valeur: 19 683 points.',
    quicksilvyr: 'Le troisième élément métallique. Valeur: 59 049 points.',
    goldNugget: 'Le dernier élément métallique. Valeur: 17 7147 points.',
    unknown: "Vous n'avez pas encore découvert cet élément.",
  },
};
