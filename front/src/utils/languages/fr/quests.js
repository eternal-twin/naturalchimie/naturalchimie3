const quests = {
  noQuest: '[aucune quête]',
  noActiveQuest: '[#totalQuests quête(s) en pause]',
  quests: {
    quest1: { name: 'foo' },
  },
};

export default quests;
