import elements from './elements';
import quests from './quests';
import tooltips from './tooltips';

const French = {
  buttons: {
    play: 'Jouer',
    move: 'Se déplacer',
  },
  mainPage: {
    magicObjects: 'objets magiques',
    magicObjectsAllowed: "L'utilisation des objets magiques personnels est autorisée dans ce lieu.",
    magicObjectsNotAllowed: "Impossible d'utiliser ses propres objets magiques ici.",
  },
  inventoryPage: {
    bagTitle: 'Mon sac à dos',
    beltTitle: "Ceinture d'alchimiste",
    beltDescription: `Stockez des objets magiques dans votre ceinture pour les utiliser en jeu, dans les lieux autorisés. 
     La plupart des objets magiques ne peuvent être placés qu'en un seul exemplaire.`,
    beltSlotLocked: `Cet emplacement est fermé. Vous pouvez le débloquer en trouvant une nouvelle poche pour votre ceinture.`,
  },
  elements: elements,
  quests: quests,
  tooltips: tooltips,
};

export default French;
