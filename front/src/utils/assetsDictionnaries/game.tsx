import playBackground from 'src/assets/img/game/play_bg.jpg';
import containerBackgroundMain from 'src/assets/img/game/client_game_main.png';
import containerBackgroundHeader from 'src/assets/img/game/client_game_header.png';
import containerBackgroundRoll from 'src/assets/img/game/client_game_rep.png';
import containerBackgroundFooter from 'src/assets/img/game/client_game_bot.png';
import elementChainBackground from 'src/assets/img/game/gameChainRedim.png';
import nextPieceBackground from 'src/assets/img/game/nextPieceBackground.png';
import nextPieceForeground from 'src/assets/img/game/nextPieceForeground.png';

import itemBeltBackground from 'src/assets/img/game/belt_bg.png';
import itemBeltPocket1 from 'src/assets/img/game/pocket_1.png';
import itemBeltPocket2 from 'src/assets/img/game/pocket_2.png';
import itemBeltPocket3 from 'src/assets/img/game/pocket_3.png';
import itemBeltPocket4 from 'src/assets/img/game/pocket_4.png';

import boardBackground from 'src/assets/img/game/gameboard_bg.svg';
import boardForeground from 'src/assets/img/game/gameboard_foreground.png';

// eslint-disable-next-line
const assetsDictionnaryGame: Record<string, any> = {
  elementChainBackground: elementChainBackground,
  nextPieceBackground: nextPieceBackground,
  nextPieceForeground: nextPieceForeground,
  playBackground: playBackground,
  belt: {
    background: itemBeltBackground,
    pocket1: itemBeltPocket1,
    pocket2: itemBeltPocket2,
    pocket3: itemBeltPocket3,
    pocket4: itemBeltPocket4,
  },
  container: {
    backgroundFooter: containerBackgroundFooter,
    backgroundHeader: containerBackgroundHeader,
    backgroundMain: containerBackgroundMain,
    backgroundRoll: containerBackgroundRoll,
  },
  gameboard: {
    background: boardBackground,
    foreground: boardForeground,
  },
};

export default assetsDictionnaryGame;
