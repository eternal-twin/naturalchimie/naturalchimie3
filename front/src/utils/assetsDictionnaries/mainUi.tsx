import mainBackground from 'src/assets/img/ui/bg.jpg';

import pyram from 'src/assets/img/ui/leftMenu/pyram.svg';
import kubor from 'src/assets/img/ui/leftMenu/kubor.svg';
import currencyCardBackground from 'src/assets/img/ui/leftMenu/CurrencyCardBackground.svg';
import avatarForeground from 'src/assets/img/ui/leftMenu/AvatarForeground.svg';
import xpFlask from 'src/assets/img/ui/leftMenu/XpFlask.svg';
import questScroll from 'src/assets/img/ui/leftMenu/quest_bg_transparent.png';
import subMenuBackground from 'src/assets/img/ui/leftMenu/sub_menu_bg_transparent.png';
import personalPageIcon from 'src/assets/img/ui/leftMenu/sub_menu_base.png';
import grimoireIcon from 'src/assets/img/ui/leftMenu/sub_menu_grim.png';
import classIcon from 'src/assets/img/ui/leftMenu/sub_menu_school.png';
import crystalBallIcon from 'src/assets/img/ui/leftMenu/sub_menu_fo.png';
import inventoryIcon from 'src/assets/img/ui/leftMenu/sub_menu_inventory.png';

import iconPlay from 'src/assets/img/buttons/icon_play.png';
import iconMove from 'src/assets/img/buttons/icon_move.png';
import iconRank from 'src/assets/img/buttons/icon_rank.png';
import iconMoveDown from 'src/assets/img/buttons/icon_down.png';
import iconMoveUp from 'src/assets/img/buttons/icon_up.png';
import iconTalk from 'src/assets/img/buttons/icon_talk.png';
import iconShop from 'src/assets/img/buttons/icon_shop.png';
import iconSleep from 'src/assets/img/buttons/icon_sleep.png';

import headerMenu from 'src/assets/img/ui/header_v1.png';
import headerLogo from 'src/assets/img/ui/headerLogo.png';
import mapMenuIcon from 'src/assets/img/ui/mainMenu_map_textless.png';
import bankMenuIcon from 'src/assets/img/ui/mainMenu_bank_textless.png';
import cexMenuIcon from 'src/assets/img/ui/mainMenu_cex_textless.png';
import helpMenuIcon from 'src/assets/img/ui/mainMenu_help_textless.png';
import guildianMenuIcon from 'src/assets/img/ui/mainMenu_guildian_textless.png';
import arrowLeft from 'src/assets/img/buttons/snb_prev.gif';
import arrowRight from 'src/assets/img/buttons/snb_next.gif';
import chainedBorder from 'src/assets/img/ui/text-chain-border.svg';

import elementsCard from 'src/assets/img/ui/cartouche_elem.png';

import illustrationForeground from 'src/assets/img/ui/illustration_foreground.png';

import iconAp from 'src/assets/img/schools/ap.gif';
import smallIconAp from 'src/assets/img/schools/ap_small.gif';
import iconSk from 'src/assets/img/schools/sk.gif';
import smallIconSk from 'src/assets/img/schools/sk_small.gif';
import iconJz from 'src/assets/img/schools/jz.gif';
import smallIconJz from 'src/assets/img/schools/jz_small.gif';
import iconGm from 'src/assets/img/schools/gm.gif';
import smallIconGm from 'src/assets/img/schools/gm_small.gif';
import iconGu from 'src/assets/img/schools/gu.gif';
import smallIconGu from 'src/assets/img/schools/gu_small.gif';

const assetsDictionnaryMainPage: Record<string, any> = {
  arrowLeft,
  arrowRight,
  buttonIcons: {
    play: iconPlay,
    move: iconMove,
    moveDown: iconMoveDown,
    moveUp: iconMoveUp,
    rank: iconRank,
    sleep: iconSleep,
    shop: iconShop,
    talk: iconTalk,
  },
  menuIcons: {
    bank: bankMenuIcon,
    cex: cexMenuIcon,
    guildian: guildianMenuIcon,
    help: helpMenuIcon,
    map: mapMenuIcon,
  },
  schools: {
    icon: {
      audepint: iconAp,
      gemini: iconGm,
      guild: iconGu,
      jeezara: iconJz,
      shangKah: iconSk,
    },
    smallIcon: {
      audepint: smallIconAp,
      gemini: smallIconGm,
      guild: smallIconGu,
      jeezara: smallIconJz,
      shangKah: smallIconSk,
    },
  },
  leftMenu: {
    currencies: {
      background: currencyCardBackground,
      kubor: kubor,
      pyram: pyram,
    },
    avatarForeground: avatarForeground,
    xpFlask: xpFlask,
    questScroll: questScroll,
    subMenuBackground: subMenuBackground,
    menu: {
      classIcon: classIcon,
      crystalBallIcon: crystalBallIcon,
      grimoireIcon: grimoireIcon,
      inventoryIcon: inventoryIcon,
      personalPageIcon: personalPageIcon,
    },
  },
  header: {
    arrowLeft: arrowLeft,
    arrowRight: arrowRight,
    headerMenu: headerMenu,
    headerLogo: headerLogo,
    menuIcons: {
      bank: bankMenuIcon,
      cex: cexMenuIcon,
      guildian: guildianMenuIcon,
      help: helpMenuIcon,
      map: mapMenuIcon,
    },
  },
  rightMenu: {
    buttonIcons: {
      play: iconPlay,
      move: iconMove,
      moveDown: iconMoveDown,
      moveUp: iconMoveUp,
      rank: iconRank,
      sleep: iconSleep,
      shop: iconShop,
      talk: iconTalk,
    },
  },
  elementsCard,
  headerMenu,
  headerLogo,
  illustrationForeground,
  mainBackground,
  chainedBorder: chainedBorder,
};

export default assetsDictionnaryMainPage;
