import element0 from 'src/assets/img/elements/elt0.png';
import element1 from 'src/assets/img/elements/elt1.png';
import element2 from 'src/assets/img/elements/elt2.png';
import element3 from 'src/assets/img/elements/elt3.png';
import element4 from 'src/assets/img/elements/elt4.png';
import element5 from 'src/assets/img/elements/elt5.png';
import element6 from 'src/assets/img/elements/elt6.png';
import element7 from 'src/assets/img/elements/elt7.png';
import element8 from 'src/assets/img/elements/elt8.png';
import element9 from 'src/assets/img/elements/elt9.png';
import element10 from 'src/assets/img/elements/elt10.png';
import element11 from 'src/assets/img/elements/elt11.png';
import unknown from 'src/assets/img/elements/unknown.png';

// eslint-disable-next-line
const assetsDictionnaryElements: Record<string, any> = {
  element0: element0,
  element1: element1,
  element2: element2,
  element3: element3,
  element4: element4,
  element5: element5,
  element6: element6,
  element7: element7,
  element8: element8,
  element9: element9,
  element10: element10,
  element11: element11,
  unknown: unknown,
};

export default assetsDictionnaryElements;
