import prairieBackground from 'src/assets/img/locations/gmprai.jpg';

// eslint-disable-next-line
const assetsDictionnaryLocationBackgrounds: Record<string, any> = {
  prairieBackground: prairieBackground,
};

export default assetsDictionnaryLocationBackgrounds;
