import bagBackground from 'src/assets/img/inventory/bag.png';

import bagHeader from 'src/assets/img/inventory/bag_top.png';
import bagFooter from 'src/assets/img/inventory/bag_bottom.png';
import bagBody from 'src/assets/img/inventory/bag_body.png';

import belt from 'src/assets/img/inventory/belt_Bg.gif';
import beltSlot from 'src/assets/img/inventory/belt_slot.gif';
import itemSelector from 'src/assets/img/inventory/Selector.png';

// eslint-disable-next-line
const assetsDictionnaryInventoryPage: Record<string, any> = {
  bagBackground,
  bag: {
    bagBody,
    bagFooter,
    bagHeader,
  },
  belt,
  beltSlot,
  itemSelector,
};

export default assetsDictionnaryInventoryPage;
