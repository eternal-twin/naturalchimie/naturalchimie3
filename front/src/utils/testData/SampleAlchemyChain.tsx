import { AlchemyElement, AlchemyElementChain } from 'src/types';
import assetsDictionnary from '../assetsDictionnary';

//TODO: to remove when backend generates pieces
const ElementNames: Array<string> = [
  'greenPotion',
  'yellowPotion',
  'orangePotion',
  'purplePotion',
  'mentholLeaf',
  'crazeye',
  'skullie',
  'flawer',
  'oxide',
  'coppa',
  'quicksilvyr',
  'goldNugget',
];

//TODO: to remove when backend generates pieces
const generateElementChain = (formatMessage: (arg0: { id: string }) => string): AlchemyElementChain => {
  const res: AlchemyElementChain = [];
  for (let i = 0; i < 12; i++) {
    const elem: AlchemyElement = {
      id: i,
      name: formatMessage({ id: 'elements.name.' + ElementNames[i] }),
      description: formatMessage({ id: 'elements.description.' + ElementNames[i] }),
      rank: i,
      value: Math.pow(3, i),
      img: assetsDictionnary['elements.element' + i],
      usable: false,
    };
    res.push(elem);
  }
  return res;
};

export default generateElementChain;
