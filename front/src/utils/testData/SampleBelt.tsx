import { Belt } from 'src/types';
import assetsDictionnary from '../assetsDictionnary';

const generateItemBelt = (formatMessage: (arg0: { id: string }) => string): Belt => {
  return [
    {
      isLocked: false,
      item: {
        id: 2,
        name: formatMessage({ id: 'elements.name.yellowPotion' }),
        description: formatMessage({ id: 'elements.description.yellowPotion' }),
        img: assetsDictionnary['elements.element1'],
        equippable: true,
        usable: false,
      },
      pocketIndex: 1,
    },
    {
      isLocked: false,
      item: {
        id: 3,
        name: formatMessage({ id: 'elements.name.orangePotion' }),
        description: formatMessage({ id: 'elements.description.orangePotion' }),
        img: assetsDictionnary['elements.element2'],
        equippable: true,
        usable: false,
      },
      pocketIndex: 2,
    },
    { isLocked: true, item: null, pocketIndex: 3 },
    { isLocked: true, item: null, pocketIndex: 4 },
  ];
};

export default generateItemBelt;
