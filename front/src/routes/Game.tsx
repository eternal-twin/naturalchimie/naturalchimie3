import React from 'react';
import GameLayout from 'src/components/layouts/GameLayout';

const Game: React.FC = () => {
  return <GameLayout />;
};

export default Game;
