import React from 'react';
import HomeLayout from '../components/layouts/HomeLayout';

const Home: React.FC = () => {
  return <HomeLayout />;
};

export default Home;
