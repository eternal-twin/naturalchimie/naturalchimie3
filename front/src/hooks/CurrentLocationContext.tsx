import React, { createContext, useContext, useEffect, useMemo, useState } from 'react';
import { AlchemyElementChain } from '../types';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useIntl } from 'react-intl';
import generateLocation from 'src/utils/testData/SampleLocation';

export interface CurrentLocationContextType {
  name: string;
  description: string;
  background: string;
  playerElements: AlchemyElementChain;
  completeElements: AlchemyElementChain;
  isSchoolCup: boolean;
  areMagicItemsAllowed: boolean;
  faction: string;
}

export const CurrentLocationContext = createContext<CurrentLocationContextType | null>(null);

export const CurrentLocationProvider: React.FC = ({ children }) => {
  const { formatMessage } = useIntl();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [background, setBackground] = useState<string>(assetsDictionnary['locations.prairieBackground']);
  const [playerElements, setPlayerElements] = useState<AlchemyElementChain>([]); //TODO: may come from userContext instead?
  const [completeElements, setCompleteElements] = useState<AlchemyElementChain>([]);
  const [isSchoolCup, setIsSchoolCup] = useState<boolean>(false);
  const [areMagicItemsAllowed, setAreMagicItemsAllowed] = useState<boolean>(false);
  const [faction, setFaction] = useState<string>('guild');

  useEffect(() => {
    const location = generateLocation(formatMessage);
    setName(location.name);
    setDescription(location.description);
    setPlayerElements(location.playerElements);
    setCompleteElements(location.completeChain);
    setBackground(location.background);
    setIsSchoolCup(location.isSchoolCup);
    setAreMagicItemsAllowed(location.isSchoolCup || location.areMagicItemsAllowed);
  }, []);

  const changeLocation = (newLocation: CurrentLocationContextType) => {
    setName(newLocation.name);
    setDescription(newLocation.description);
    setPlayerElements(newLocation.playerElements);
    setCompleteElements(newLocation.completeElements);
    setIsSchoolCup(true);
  };

  const values = useMemo(
    () => ({
      background,
      name,
      description,
      playerElements,
      completeElements,
      isSchoolCup,
      areMagicItemsAllowed,
      faction,
      changeLocation,
    }),
    [background, name, description, playerElements, completeElements, isSchoolCup, areMagicItemsAllowed, faction]
  );

  return <CurrentLocationContext.Provider value={values}>{children}</CurrentLocationContext.Provider>;
};

export const useCurrentLocation = (): CurrentLocationContextType => {
  const context = useContext(CurrentLocationContext);
  if (context === undefined && context === null) {
    throw new Error('useCurrentLocation is missing CurrentLocationContext');
  }
  return context as CurrentLocationContextType;
};

export default CurrentLocationProvider;
