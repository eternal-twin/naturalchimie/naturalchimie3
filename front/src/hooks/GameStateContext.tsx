import React, { createContext, useContext, useMemo, useState } from 'react';
import { useIntl } from 'react-intl';
import { DroppedPieces, AlchemyElement, BeltSlot, GameItem } from 'src/types';
import assetsDictionnary from 'src/utils/assetsDictionnary';
import { useUserContext } from './userContext';

//TODO: to remove when backend generates pieces
const ElementNames: Array<string> = [
  'greenPotion',
  'yellowPotion',
  'orangePotion',
  'purplePotion',
  'mentholLeaf',
  'crazeye',
  'skullie',
  'flawer',
  'oxide',
  'coppa',
  'quicksilvyr',
  'goldNugget',
];

//TODO: to remove when backend generates pieces
const getRandomElement = (max: number, formatMessage: (arg0: { id: string }) => string): AlchemyElement => {
  const rank = Math.floor(Math.random() * Math.floor(max)) + 1;
  return {
    id: rank,
    name: formatMessage({ id: 'elements.name.' + ElementNames[rank] }),
    description: formatMessage({ id: 'elements.description.' + ElementNames[rank] }),
    rank: rank,
    value: Math.pow(3, rank),
    img: assetsDictionnary['elements.element' + rank],
    usable: false,
  };
};

//TODO: to remove when backend generates pieces
const generateDummyPieces = (formatMessage: (arg0: { id: string }) => string): Array<Array<AlchemyElement>> => {
  const n = Math.floor(Math.random() * Math.floor(4)) + 1;
  let results: Array<AlchemyElement> = [];
  for (let i = 0; i < n; i++) {
    results = [...results, getRandomElement(10, formatMessage)];
  }
  return [results];
};

export interface GameStateContextTypes {
  boardState: Array<Array<AlchemyElement | GameItem | null>>;
  nextPieces: Array<Array<AlchemyElement | GameItem>>;
  score: number;
  setScore: (newScore: number) => void;
  apaScore: number;
  lastPlayedPieces: DroppedPieces | undefined;
  pairDroppedEvent: (droppedPair: DroppedPieces) => void;
  useItemFromBelt: (usedItem: BeltSlot) => void;
}

//beltItems.map((item, index) => index === itemUsedIndex ?

export const GameStateContext = createContext<GameStateContextTypes | null>(null);

export const GameStateProvider: React.FC = ({ children }) => {
  const { formatMessage } = useIntl();
  const [score, setScore] = useState(0);
  const [apaScore, setApaScore] = useState(50000);
  const [nextPieces, setNextPieces] = useState<Array<AlchemyElement | GameItem>[]>([[]]);
  const [currentPieces, setCurrentPieces] = useState<Array<AlchemyElement | GameItem>>([]);
  const [boardState, setBoardState] = useState<Array<Array<AlchemyElement | GameItem | null>>>([[]]);
  const [lastPlayedPieces, setLastPlayedPieces] = useState<DroppedPieces | undefined>();
  const { removeUsedItemFromBelt } = useUserContext();

  const startNewGame = () => {
    //call backend, remove 1 pyram, get a new pair of element
  };

  const pairDroppedEvent = (droppedPair: DroppedPieces) => {
    setLastPlayedPieces(droppedPair);
    //call backend with droppedPair & boardState. Get new nextPair back
    // calculate new boardState
    // set score
    //setNextPair(await backendCall)
    setNextPieces(generateDummyPieces(formatMessage));
  };

  const useItemFromBelt = (usedItem: BeltSlot) => {
    if (usedItem.item != null) setNextPieces([[usedItem.item], ...nextPieces]);
    removeUsedItemFromBelt(usedItem.pocketIndex);
  };

  const values = useMemo(
    () => ({
      score,
      setScore,
      apaScore,
      nextPieces,
      boardState,
      lastPlayedPieces,
      currentPieces,
      pairDroppedEvent,
      startNewGame,
      useItemFromBelt,
    }),
    [score, apaScore, nextPieces, currentPieces, boardState, lastPlayedPieces]
  );
  return <GameStateContext.Provider value={values}>{children}</GameStateContext.Provider>;
};

export const useGameState = (): GameStateContextTypes => {
  const context = useContext(GameStateContext);
  if (context === undefined && context === null) {
    throw new Error('useGameState is missing GameStateContext');
  }
  return context as GameStateContextTypes;
};

export default GameStateProvider;
