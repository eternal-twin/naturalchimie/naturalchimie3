export interface AlchemyElement {
  id: number;
  name?: string;
  description: string;
  value: number;
  rank: number;
  img: string;
  usable: boolean;
}

export interface GameItem {
  id: number;
  name: string;
  description: string;
  equippable: boolean;
  usable: boolean;
  img: string;
  effect?: () => void;
}

export type Inventory = Array<{
  count: number;
  availableForBelt?: number;
  item: AlchemyElement | GameItem;
}>;

export const isGameItem = (item: AlchemyElement | GameItem): item is GameItem => {
  if ((item as GameItem)?.equippable != undefined) {
    return true;
  } else {
    return false;
  }
};

export const isAlchemyElement = (item: AlchemyElement | GameItem): item is AlchemyElement => {
  if ((item as AlchemyElement)?.value != undefined) {
    return true;
  } else {
    return false;
  }
};

export type AlchemyElementChain = AlchemyElement[];

export type DroppedPieces = [
  { element: AlchemyElement | GameItem; position: number },
  { element: AlchemyElement | GameItem; position: number }
];

export type BeltSlot = {
  isLocked: boolean;
  pocketIndex: number;
  item: GameItem | null;
};

export type Belt = Array<BeltSlot>;

export type Quest = {
  name: string;
  steps: [
    {
      id: number;
      description: string;
      isCompleted: boolean;
      requirement?: [{ item: Element; total: number; gathered: number }]; //TODO
    }
  ];
  isActive: boolean;
};

export enum Faction {
  GUILD = 'Guilde',
  SHANGKAH = 'Shang-Kah',
  AUDEPINT = 'Audepint',
  GEMINI = 'Gemini',
  JEEZARA = 'Jeezara',
}

export interface Location {
  name: string;
  description: string;
  playerElements: AlchemyElementChain;
  completeChain: AlchemyElementChain;
  background: string;
  isSchoolCup: boolean;
  areMagicItemsAllowed: boolean;
}
