import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import LocaleContextProvider from './hooks/IntlContext';
import './index.scss';
import Theme from './styles/theme';
import GlobalFonts from 'src/styles/fonts';

ReactDOM.render(
  <LocaleContextProvider>
    <Theme>
      <GlobalFonts />
      <App />
    </Theme>
  </LocaleContextProvider>,
  document.getElementById('app')
);
