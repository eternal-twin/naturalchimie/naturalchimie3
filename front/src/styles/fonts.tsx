import assetsDictionnary from 'src/utils/assetsDictionnary';
import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    @font-face {
        font-family:"LondonTwo";
        src:url(${assetsDictionnary['fonts.scoreLayerEot']}) format("eot"),
            url(${assetsDictionnary['fonts.scoreLayerWoff']}) format("woff"),
            url(${assetsDictionnary['fonts.scoreLayerTtf']}) format("truetype"),
            url(${assetsDictionnary['fonts.scoreLayerSvg']}) format("svg");
        font-weight:normal;
        font-style:normal;
    }
   `;
